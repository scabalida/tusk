<?php

	class m_auction extends MY_Model
	{
		protected $_table_name = 'auction_items';
		protected $_order_by = 'date_added';
		
		public function __construct()
		{
			parent::__construct();
		}

		public function getMyAuctionItems()
		{
			$this->db->select('*');
			$this->db->where('user_id', $this->session->userdata('id'));
			$this->db->from('auction_items');
			$this->db->order_by('date_added', 'DESC');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getLatestAuctions()
		{
			$today = date("Y-m-d H:i:s");
			$this->db->select('*');
			$this->db->from('auction_items');
			$this->db->where('date_expired >=', $today);
			$this->db->where('status', 1);
			$this->db->order_by('date_added', 'DESC');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getAuctionsByCategory($category)
		{
			$today = date("Y-m-d H:i:s");
			$this->db->select('*');
			$this->db->where('category', $category);
			$this->db->where('date_expired >=', $today);
			$this->db->where('status', 1);
			$this->db->from('auction_items');
			$this->db->order_by('date_added', 'DESC');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getLastDayAuction()
		{
			$date = date('Y-m-d', strtotime('+1 day'));
			$minvalue = $date . " 00:00:00";
			$maxvalue = $date . " 23:59:59";
			
			$this->db->select('*');
			$this->db->where('date_expired >=', $minvalue);
			$this->db->where('date_expired <=', $maxvalue);
			$this->db->where('status', 1);
			$this->db->from('auction_items');
			$this->db->order_by('date_added', 'ASC');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getLastAuctionByCategory($category)
		{
			$date = date('Y-m-d', strtotime('+1 day'));
			$minvalue = $date . " 00:00:00";
			$maxvalue = $date . " 23:59:59";
			
			$this->db->select('*');
			$this->db->where('date_expired >=', $minvalue);
			$this->db->where('date_expired <=', $maxvalue);
			$this->db->where('category', $category);
			$this->db->where('status', 1);
			$this->db->from('auction_items');
			$this->db->order_by('date_added', 'DESC');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getBuyerInfo($id)
		{
			$this->db->select('buyer_id');
			$this->db->where('payed', 1);
			$this->db->where('auction_id', $id);
			$this->db->from('sold_items');
			
			$query = $this->db->get();
			
			$u_id = ($query->result())[0]->buyer_id;
			
			$query2 = $this->db->get_where('user_info', array("id" => $u_id));

			return $query2->result();
		
		}
		public function getAuctionData($id)
		{
			$this->db->select('*');
			$this->db->where('u_id', $id);
			$this->db->from('auction_items');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getAuctionDataWithBidders($id)
		{
			$this->db->select('*');
			$this->db->where('u_id', $id);
			$this->db->from('auction_items');
			
			$query = $this->db->get();
			$row = $query->result();
			
			$this->db->select('*');
			$this->db->where('auction_id', $id);
			$this->db->limit(5); 
			$this->db->order_by('bidder_price', 'DESC');
			$this->db->from('bidders');
			
			
			$query2 = $this->db->get();
			$bidders = array();
			foreach($query2->result() as $row2){
				$bid = array(
					"bidder_id" => $row2-> bidder_id,
					"bidder_price" => $row2-> bidder_price
				);
				array_push($bidders, $bid);
			}
			array_push($row, $bidders);
			
			//array_push($row, $bidders);
			return $row;
		
		}
		public function getAllFeaturedAuction()
		{
			$today = date("Y-m-d H:i:s");
			$this->db->select('*');
			$this->db->where('featured', 1);
			$this->db->where('status', 1);
			$this->db->where('date_expired >=', $today);
			$this->db->limit(8); 
			$this->db->order_by('date_added', 'DESC');
			$this->db->from('auction_items');
			
			$query = $this->db->get();
			
			return $query->result();
		
		}
		public function getAllRecentAuction()
		{
			$today = date("Y-m-d H:i:s");
			$this->db->select('*');
			$this->db->where('status', 1);
			$this->db->where('date_expired >=', $today);
			$this->db->limit(4); 
			$this->db->order_by('date_added', 'DESC');
			$this->db->from('auction_items');
			
			$query = $this->db->get();
			
			return $query->result();
		
		}
		public function getRandomAuctions()
		{
			$today = date("Y-m-d H:i:s");

			$this->db->select('*');
			$this->db->where('status', 1);
			$this->db->where('date_expired >=', $today);
			$this->db->order_by('rand()');
			$this->db->limit(6); 
			$this->db->from('auction_items');
			$query = $this->db->get();
			return $query->result();
		}
		public function getTopFiveBidders($id)
		{
			$this->db->select('*');
			$this->db->where('auction_id', $id);
			$this->db->order_by('date_bid', 'DESC');
			$this->db->limit(5); 
			$this->db->from('bidders');
			$query = $this->db->get();
			return $query->result();
		}
	}