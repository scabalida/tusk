<?php

	class m_admin extends MY_Model
	{
		protected $_table_name = 'user_info';
		protected $_order_by = 'date_registered';
		
		public function __construct()
		{
			parent::__construct();
		}

		public function getAllUsers()
		{
			$this->db->select('id, fullname, email, bday, address, contact1, cc_code, zip, country, date_registered, date_expired, verified, role');
			$this->db->where('role !=', 2);
			$this->db->where('id !=', $this->session->userdata('id'));
			$this->db->from('user_info');
			$this->db->order_by('date_registered', 'DESC');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function deleteUser($id)
		{	
			$query = $this->db->delete('user_info', array('id' => $id)); 

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function get_visitors()
		{
			$query = $this->db->get('visitor_counter');
			return count($query->result());
		}
		public function save_visitor($ip)
		{
			$query = $this->db->get_where('visitor_counter', array('ip_address' => $ip));
			if(count($query->result()) == 0){
				$this->db->insert('visitor_counter', array('ip_address' => $ip));
			}
		}
	}