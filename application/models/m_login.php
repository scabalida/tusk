<?php

	class m_login extends MY_Model
	{
		protected $_table_name = 'user_info';
		protected $_order_by = 'date_registered';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function login($email, $pass)
		{
			$user = $this->get_by( 

				array (
					'email' => $email,
					'password' => $pass,
				));
			

			if($user)
			{
					if($user[0]->date_expired != "0000-00-00"){
						if(strtotime($user[0]->date_expired) >= strtotime(date("Y-m-d"))){
							foreach ($user as $row)
							{
								$data = array (
									'id'   			=> $row->id,
									'fullname' 		=> $row->fullname,
									'contact1' 		=> $row->contact1,
									'cc_code' 		=> $row->cc_code,
									'country' 		=> $row->country,
									'zip' 		=> $row->zip,
									'email' 			=> $row->email,
									'date_registered' 	=> $row->date_registered,
									'date_expired' 	=> $row->date_expired,
									'role' 			=> $row->role,
									'loggedin' 			=> TRUE
								);
								$this->session->set_userdata($data);
							}
							return 0; //perfect
						}else{
							return 2; //renew account
						}
					}else{
						return 1; //not yet paid
					}
			}
			else{
				return 3; //wrong credentials
				
			}
		}
		public function logout()
		{
			$this->session->set_userdata(array());
			$this->session->sess_destroy();
			//redirect('login');
			redirect(base_url(),'refresh');
		}

	}