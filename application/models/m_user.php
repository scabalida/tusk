<?php

	class m_user extends MY_Model
	{
		protected $_table_name = 'user_info';
		protected $_order_by = 'date_registered';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getUserInfo()
		{	
			$query = $this->db->get_where('user_info', array("id" => $this->session->userdata('id')));

			return $query->result();
		
		}
		public function updateUserInfo($data,$id)
		{	
			$query = $this->db->update('user_info',$data, array("id" => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function getUserData($id)
		{
			$this->db->select('fullname ,email ,contact1 ,cc_code ,zip ,country');
			$this->db->where('id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getUserExpired($id)
		{
			$this->db->select('date_expired');
			$this->db->where('id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getUserID($email)
		{
			$this->db->select('id');
			$this->db->where('email', $email);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getUserPass()
		{	
			$id = $this->session->userdata('u_id');
			$this->db->select('u_password');
			$this->db->where('u_id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function chckEmail($email)
		{
			$this->db->select('email');
			$this->db->where('email', $email);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			if(count($query->result()) > 0){
				return true;
			}else{
				return false;
			}
		
		}
	}