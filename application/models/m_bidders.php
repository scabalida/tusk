<?php

	class m_bidders extends MY_Model
	{
		protected $_table_name = 'bidders';
		protected $_order_by = 'date_bid';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getBidders($id)
		{
			$this->db->select('*');
			$this->db->where('auction_id', $id);
			$this->db->from('bidders');
			$this->db->order_by('bidder_price', 'DESC');
			
			$query = $this->db->get();
			$newArray = array();
			if(count($query->result()) > 0){
				foreach($query->result() as $row){
					$getName = $this->db->get_where('user_info', array("id" => $row->bidder_id));
					foreach($getName->result() as $row2){
						$data = array(
							"u_id" 			=> $row->u_id,
							"auction_id" 	=> $row->auction_id,
							"bidder_id" 	=> $row->bidder_id,
							"bidder_price" 	=> $row->bidder_price,
							"date_bid" 		=> $row->date_bid,
							"fullname" 		=> ucwords($row2->fullname)
						);
						array_push($newArray,$data);
					}
				}
				return $newArray;
			}else{
				return false;
			}
		
		}
	}