<?php

	class m_sold extends MY_Model
	{
		protected $_table_name = 'sold_items';
		protected $_order_by = 'date_sold';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getBoughtItems()
		{
			$this->db->select('*');
			$this->db->where('buyer_id', $this->session->userdata('id'));
			$this->db->from('sold_items');
			$this->db->order_by('date_sold', 'DESC');
			
			$query = $this->db->get();
			$newArray = array();
			foreach($query->result() as $row){
				$query2 = $this->db->get_where('auction_items', array("u_id" => $row->auction_id));
				foreach($query2->result() as $row2){
					$query3 = $this->db->get_where('user_info', array("id" => $row->seller_id));	
					foreach($query3->result() as $row3){
						$data = array(
							"bought_id" => $row->u_id,
							"buyer_id" => $row->buyer_id,
							"bid_price" => $row->bid_price,
							"seller_id" => $row->seller_id,
							"date_sold" => $row->date_sold,
							"item_name" => $row2->item_name,
							"item_desc" => $row2->item_desc,
							"designer_name" => $row2->designer_name,
							"category" => $row2->category,
							"featured" => $row2->featured,
							"main_pic" => $row2->main_pic,
							"other_pic1" => $row2->other_pic1,
							"other_pic2" => $row2->other_pic2,
							"other_pic3" => $row2->other_pic3,
							"fullname" => $row3->fullname,
							"email" => $row3->email,
							"contact1" => $row3->contact1,
							"cc_code" => $row3->cc_code,
							"zip" => $row3->zip,
							"country" => $row3->country,
							"address" => $row3->address
						);
						
						array_push($newArray, $data);
					}
				}
			}
			
			return $newArray;
		
		}
	}	