<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>

<body style="font-family:Arial">
<center>
	<p style="color:#e65045;font-size:46px;margin-bottom:0">CONGRATULATIONS!!</p>
	<p style="color:#4c4a49;font-size:25px;margin-top:0">Auction item has been sold.</p>
	<p><strong>Item Name: <span style="color:#e65045;"><?php echo $item_name; ?></span></strong></p>
	<img style="max-height:300px;width:auto;" src="<?php echo site_url("images/auctions/" . $seller_id . "/" . $main_pic); ?>" />
	<p><strong>Bid Win Price: <span style="color:#e65045;">Kr.<?php echo $bid_price; ?></span></strong></p>
	<br/>
	
	<table>
		<thead>
			<tr>
				<th><u>Seller's Information</u></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><strong>Full Name</strong></td>
				<td><span style="color:#af1106;"><?php echo $seller_name; ?></span></td>
			</tr>
			<tr>
				<td><strong>Email Address</strong></td>
				<td><span style="color:#af1106;"><?php echo $seller_email; ?></span></td>
			</tr>
			<tr>
				<td><strong>Contact Number</strong></td>
				<td><span style="color:#af1106;"><?php echo "(" . $seller_cc_code . ") " . $seller_contact1 ?></span></td>
			</tr>
			<tr>
				<td><strong>Zip</strong></td>
				<td><span style="color:#af1106;"><?php echo $seller_zip; ?></span></td>
			</tr>
			<tr>
				<td><strong>Country</strong></td>
				<td><span style="color:#af1106;"><?php echo $seller_country; ?></span></td>
			</tr>
		</tbody>
	</table>
	<br/>
	<table>
		<thead>
			<tr>
				<th><u>Bidder's Information</u></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><strong>Full Name</strong></td>
				<td><span style="color:#af1106;"><?php echo $buyer_name; ?></span></td>
			</tr>
			<tr>
				<td><strong>Email Address</strong></td>
				<td><span style="color:#af1106;"><?php echo $buyer_email; ?></span></td>
			</tr>
			<tr>
				<td><strong>Contact Number</strong></td>
				<td><span style="color:#af1106;"><?php echo "(" . $buyer_cc_code . ") " . $buyer_contact1 ?></span></td>
			</tr>
			<tr>
				<td><strong>Zip</strong></td>
				<td><span style="color:#af1106;"><?php echo $buyer_zip; ?></span></td>
			</tr>
			<tr>
				<td><strong>Country</strong></td>
				<td><span style="color:#af1106;"><?php echo $buyer_country; ?></span></td>
			</tr>
		</tbody>
	</table>
	<br/>
<br/>
<p style="color:gray;font-size:15px;">© 2019 Tusk</p>
<br/>
<br/>
</center>

</body>

</html>