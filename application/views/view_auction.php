<style>
	.img-w-h{height: 235px;width: 100%;}
	.img-slide-w{height: 460px;width: auto;    margin: 0 auto;}
	.shadow{    text-shadow: 2px 1px #777;}
	.shadow2{    text-shadow: 1px 1px #222;}
	.text-white{color:#fff;}
	.display-none{display:none}
	.flip-clock-wrapper ul li a div div.inn {
				    background-color: transparent;
    background-repeat: no-repeat;
    background-image: url('../../images/jewel.jpg');
    background-position: bottom;
    background-size: cover;
			}
	.recent_count,
	.btn_count{
		    opacity: 0;
    filter: alpha(opacity=0);
    border: 0;
    padding: 0;
    margin: 0;
	}
</style>
            <!-- BLOCKS -->
            <div class="block-main container no-padding-top">
               <div class="row">
                        <div class="col-md-5 col-sm-6">
                            <div class="owl-carousel sync1">
								<?php if($auction[0]->main_pic != NULL) { ?>
									<div class="item" id="pic-1"> <img id="pic-img-1" src="<?php echo site_url("images/auctions/" . $this->session->userdata('id') . "/" . $auction[0]->main_pic); ?>" alt=""> </div>
								<?php } ?>
								<?php if($auction[0]->other_pic1 != NULL) { ?>
									<div class="item" id="pic-2"> <img id="pic-img-2" src="<?php echo site_url("images/auctions/" . $this->session->userdata('id') . "/" . $auction[0]->other_pic1); ?>" alt=""> </div>
                                <?php } ?>
								<?php if($auction[0]->other_pic2 != NULL) { ?>
									<div class="item" id="pic-3"> <img id="pic-img-3" src="<?php echo site_url("images/auctions/" . $this->session->userdata('id') . "/" . $auction[0]->other_pic2); ?>" alt=""> </div>
                                <?php } ?>
								<?php if($auction[0]->other_pic3 != NULL) { ?>
									<div class="item" id="pic-4"> <img id="pic-img-4" src="<?php echo site_url("images/auctions/" . $this->session->userdata('id') . "/" . $auction[0]->other_pic3); ?>" alt=""> </div>
								<?php } ?>
							</div>
                        </div>
                        <div class="col-md-7 col-sm-6">
                            <div class="product-single">
                                <div class="ps-header">
									<div id="feature_clock<?php echo $auction[0]->u_id; ?>"></div>
										<button class="btn_count" onclick="showFeatureCountdown(<?php echo (strtotime($auction[0]->date_expired) - strtotime(date("Y-m-d H:i:s"))) . "," . $auction[0]->u_id; ?>)"></button>
                                    <h3 id="auc_name"><?php echo $auction[0]->item_name; ?></h3>
                                    <div class="ps-price">Min Price <b>Kr.</b> <strong id="auc_price"><?php echo $auction[0]->min_price; ?></strong></div>
                                </div>
                                <div class="sep"></div>
                                <div class="space10"></div>
								<em>Item Description</em>
								<p id="auc_desc"><?php echo $auction[0]->item_desc; ?></p>
								<em id="auc_designer_label">Designer's Name</em>
								<p id="auc_designer"><?php echo $auction[0]->designer_name; ?></p>
								<div class="space30"></div>
								<?php if($this->session->userdata('loggedin') == TRUE){ ?>
									<div class="alert alert-success display-none" id="addBidSuccess">
										<p><strong>Success!</strong> Your bid is now added.</p>
									</div>

									<div class="alert alert-danger display-none" id="addBidError">
										<p><strong>Error!</strong> Error in adding your bid.</p>
									</div>
									<form id="sendbid-form"  method="post" data-toggle="validator" role="form">
										<ul class="form-list row">
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label >Bid Amount (Kr.)</label>
													<div class="help-block" id="lowBid" style="color: #a94442;display:none">Your bid must be greater than or equal to minimum price.</div>
													<input required type="number" step="0.01" name="bidder_price" id="bidder_price" class="form-control input-text">
													<input type="hidden" name="auction_id" id="auction_id" value="<?php echo $auction[0]->u_id; ?>">
													<input type="hidden" name="auction_min_price" id="auction_min_price" value="<?php echo $auction[0]->min_price; ?>">
													<div class="help-block with-errors"></div>
												</div>
											</li>
										</ul>
										<div class="buttons-set">
											<button class="btn-color" type="submit">SEND BID</button>
										</div>
									</form>
								<?php }else{ ?>
									<a href="<?php echo base_url(); ?>login" class="btn btn-primary">LOGIN TO BID</a>
								<?php } ?>
								<br/>
								<h5><u>Top Five Bids</u></h5>
								<?php if(count($bidders) > 0) {?>
									<table id="topFiveBid" class="table table-striped table-bordered fontArial" style="width:100%;">
										<thead>
											<tr>
												<th>Member ID</th>
												<th>Bid Price (Kr.)</th>
											</tr>
										</thead>
										<tbody id="tbodyTopFiveBid">
											<?php foreach($bidders as $bid){?>
												<tr>
													<td><?php echo str_pad($bid->bidder_id, 10, '0', STR_PAD_LEFT); ?></td>
													<td><?php echo $bid->bidder_price; ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								<?php }else {?>
									<div class="alert alert-info" id="notopfivebid" role="alert">
										<h4>No bidder/s.</h4>
									</div>
								<?php }?>
							</div>
                        </div>
                    </div>
			</div>
          <!-- POLICY -->
            <div class="policy-item" id="policy2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="pi-wrap">
                                <i class="fa fa-plane"></i>
                                <h4>shipping<span>Write shipping options in your auctions.</span></h4>
                                <p>Shipping, Collect or Delivery will be arranged between seller and buyer AFTER the auction is ended.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="pi-wrap">
                                <i class="fa fa-money"></i>
                                <h4>Payment<span>All payment will be between seller and buyer</span></h4>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="pi-wrap">
                                <i class="fa fa-clock-o"></i>
                                <h4>Store Hours<span>Open: 0-24-52-365</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FOOTER -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 widget-footer">
                            <img src="<?php echo base_url();?>images/logo-tusk.png" class="img-responsive space10" alt=""/>
                            <p>www.tusk.nu is the auction site for everybody. And with the lowest cost of all the auction sites that exist.</p>
                            <div class="clearfix"></div>
                            <ul class="f-social">
                                <li><a href="https://www.facebook.com/tusk.nu/" class="fa fa-facebook"></a></li>
                                <li><a href="mailto:tusk.nu@gmail.com" class="fa fa-envelope"></a></li>
                                <li><a href="https://www.instagram.com/tusk.nu/" class="fa fa-instagram"></a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-4 widget-footer">
                            <h5>Categories</h5>
                            <ul class="widget-tags">
                                <li><a href="<?php echo base_url(); ?>auction/categories/carpets-rugs-and-textiles">Carpets, rugs and textiles</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/clothing-shoes-and-accessories">Clothing</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/electronics">Electronics</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/furniture">Furniture</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/jewellery">Jewellery</a></li>
								<li><a href="<?php echo base_url(); ?>auction/categories/travel-and-experiences">Travel and experiences</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/watches-and-clocks">Watches and clocks</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-4 widget-footer">
                            <h5>Contact Us</h5>
                            <p>Email: tusk.nu@gmail.com</p>
                        </div>
                    </div>
                </div>
            </footer>

            <!-- FOOTER COPYRIGHT -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-sm-7">
                            <br>
                            <p>Copyright 2019 &middot; All rights reserved</p>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <img src="<?php echo base_url();?>images/template/basic/payment.png" class="pull-right img-responsive payment" alt=""/>
                        </div>
                    </div>
                </div>
            </div>	

        </div>

        <div id="backtotop"><i class="fa fa-chevron-up"></i></div>

        <!-- Javascript -->
        <script src="<?php echo base_url();?>js/template/jquery.js"></script>

        <!-- ADDTHIS -->
        <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-557a95e76b3e51d9" async="async"></script>
        <script type="text/javascript">
            function loadAddThis() {
                addthis.init()
            }
        </script>
        <script src="<?php echo base_url();?>js/template/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>plugin/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url();?>js/template/bs-navbar.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/isotope/isotope.pkgd.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/slick/slick.min.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/tweets/tweecool.min.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url();?>js/template/jquery.sticky.js"></script>
        <script src="<?php echo base_url();?>js/template/jquery.subscribe-better.js"></script>
        <script src="<?php echo base_url();?>js/template/jquery-ui.min.js"></script>
        <script src="<?php echo base_url();?>js/template/validator.min.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/select/jquery.selectBoxIt.min.js"></script>
        <script src="<?php echo base_url();?>js/template/main.js"></script>
        <script src="<?php echo base_url();?>js/template/flipclock.min.js"></script>
        <script src="<?php echo base_url();?>js/template/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>js/template/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo base_url();?>js/template/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url();?>js/home.js"></script>
        <?php if(current_url() == site_url('contact-us')){?>
			<script src="<?php echo base_url();?>js/contact-us.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url('account') || current_url() == site_url('account/register') || current_url() == site_url('account/edit') || current_url() == site_url('account/changepassword')){?>
			<script src="<?php echo base_url();?>js/account.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url('login')){?>
			<script src="<?php echo base_url();?>js/login.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url('admin')){?>
			<script src="<?php echo base_url();?>js/admin.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url() || current_url() == site_url('home')){?>
			<script src="<?php echo base_url();?>js/home.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url('auction/bought') || current_url() == site_url('soon-ending') || current_url() == site_url('auction') || current_url() == site_url('auction/add') || current_url() == site_url('auction/items-list')){?>
			<script src="<?php echo base_url();?>js/auction.js"></script>
		<?php } ?>
		<?php 
			$currentURL = current_url();
			$chckURL = strpos($currentURL, "categories");
			//echo $chckURL;
			if($chckURL == 23){
		?>
			<script src="<?php echo base_url();?>js/auction.js"></script>
		<?php } ?>
    </body>
</html>