<style>
	.form-list .selectboxit-container{margin:0;}
	#categorySelectBoxItContainer{display:none;}
	#featuredSelectBoxItContainer{display:none;}
	select{display:block !important;}
</style>
            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Add Auction Item</li>
                    </ul>
                </div>
            </div>


            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- HTML -->
							<div class="alert alert-success display-none" id="addAuctionItemSuccess">
								<p><strong>Success!</strong> Auction Item successfully created.</p>
							</div>

							<div class="alert alert-danger display-none" id="addAuctionItemError">
								<p><strong>Error!</strong> Auction Item unsuccessfully created.</p>
							</div>
                            <div id="account-id">
								
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>Add Auction Item Details</h4>                                                                  
                                <div class="account-form">
                                    <form id="add-auction-form"  method="post" data-toggle="validator" role="form">                                           
                                        <ul class="form-list row">
                                            <li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label >Auction Name</label>
													<input required type="text" name="item_name" id="item_name" class="form-control input-text">
													<div class="help-block with-errors"></div>
												</div>
											</li>
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Location</label>
													<input required type="text" name="item_loc" id="item_loc" class="form-control input-text">
													<div class="help-block with-errors"></div>
												</div>
											</li>
											
                                            <li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Minimum Price(Kr.)</label>
													<input type="text" class="form-control input-text" name="min_price" id="min_price" required>
													<div class="help-block with-errors"></div>
												</div>
											</li>
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Main Picture</label>
													<input type="file" class="form-control input-text" name="main_pic" id="main_pic" required>
													<div class="help-block with-errors"></div>
												</div>
											</li>
											
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Designer's Name (Optional)</label>
													<input type="text" name="designer_name" id="designer_name" class="form-control input-text">
												</div>
											</li>
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Other Picture 1</label>
													<input type="file" class="form-control input-text" name="other_pic1" id="other_pic1" required>
													<div class="help-block with-errors"></div>
												</div>
											</li>
											
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Category</label>
													<select name="category" class="form-control" id="category" required>
														<option value="1">Carpets, rugs and textiles</option>
														<option value="2">Clothing, shoes and accessories</option>
														<option value="3">Electronics</option>
														<option value="4">Furniture</option>
														<option value="5">Glass, porcelain and ceramics</option>
														<option value="6">Hobby and collectibles</option>
														<option value="7">Home and garden</option>
														<option value="8">Hunting, fishing, arms and militaria</option>
														<option value="9">Jewellery</option>
														<option value="10">Lamps and lighting</option>
														<option value="11">Paintings and sculptures</option>
														<option value="12">Silver, bronze, copper and pewter</option>
														<option value="13">Sport and leisure</option>
														<option value="14">Travel and experiences</option>
														<option value="15">Watches and clocks</option>
														<option value="16">Wine and spirits</option>
													</select>
													<div class="help-block with-errors"></div>
												</div>
											</li>
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Other Picture 2</label>
													<input type="file" class="form-control input-text" name="other_pic2" id="other_pic2">
												</div>
											</li>
											
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Description</label>
													<textarea type="text" name="item_desc" rows="3" id="item_desc" class="form-control input-text" required></textarea>
													<div class="help-block with-errors"></div>
												</div>
											</li>
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Other Picture 3</label>
													<input type="file" class="form-control input-text" name="other_pic3" id="other_pic3">
												</div>
											</li>
											
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<p><strong>Shipment</strong></p>
													<label class="checkbox-inline"><input type="checkbox" id="ship_to_international" name="ship_to_international" >International</label>
													<label class="checkbox-inline"><input type="checkbox" id="ship_to_local" name="ship_to_local" >Local</label>
													<label class="checkbox-inline"><input type="checkbox" id="pick_up" name="pick_up" >Pick up</label>
												</div>
											</li>
                                        </ul>
                                        <div class="buttons-set text-right">
                                            <button class="btn-black" type="submit"><span><span>SAVE</span></span></button>
                                        </div>
                                    </form>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>
