            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Contact Us</li>
                    </ul>
                </div>
            </div>

            <div class="space10"></div>

            <!-- BLOG CONTENT -->
            <div class="blog-content">
                <div class="container">
                    <div class="row">
                        <!-- Sidebar -->
						<div class="alert alert-success display-none" id="sendMailSuccess">
							<p><strong>Success!</strong> Thank you for your email.</p>
						</div>
						<div class="alert alert-danger display-none" id="sendMailError">
							<p><strong>Error!</strong> Please try again.</p>
						</div>
                        <aside class="col-sm-4">
                            <div class="contact-info space50">
                                <h5 class="heading space40"><span>Contact Us</span></h5>
                                <div class="media-list">
                                    <div class="media">
                                        <div class="media-body">
                                            <strong>Customer Service:</strong><br>
                                            <a href="mailto:tusk.nu@gmail.com">tusk.nu@gmail.com</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </aside>
                        <aside class="col-sm-8 space70">
                            <h5 class="heading space40"><span>Contact Form</span></h5>                        
                            <form id="sendMailForm"  method="post" data-toggle="validator" role="form">
								<div class="form-group">
									<input type="text" name="e_name" id="e_name" class="input-md form-control" placeholder="Name" required="">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input name="e_email" id="e_email" class="input-md form-control" placeholder="Email *" maxlength="100" required="" type="email">
									<div class="help-block with-errors"></div>
								</div>	
								<div class="form-group">
									<input type="text" name="e_subject" id="e_subject" class="input-md form-control" placeholder="Subject *" maxlength="100" required="">
									<div class="help-block with-errors"></div>
								</div>	
								<div class="form-group">
									<textarea name="e_message" id="e_message" rows="5" class="input-md form-control" placeholder="Message" required=""></textarea>
									<div class="help-block with-errors"></div>
								</div>	
                                <button type="submit" class="btn-black">
                                    Send a Message
                                </button>
                            </form>

                        </aside>

                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>

            <div class="clearfix space20"></div>
