<style>
	.img-w-h{height: 235px;width: 100%;}
	.img-slide-w{height: 460px;width: auto;    margin: 0 auto;}
	.shadow{    text-shadow: 2px 1px #777;}
	.shadow2{    text-shadow: 1px 1px #222;}
	.text-white{color:#fff;}
	.display-none{display:none}
	.flip-clock-wrapper ul li a div div.inn {
				    background-color: transparent;
    background-repeat: no-repeat;
    background-image: url('images/jewel.jpg');
    background-position: bottom;
    background-size: cover;
			}
	.recent_count,
	.btn_count{
		    opacity: 0;
    filter: alpha(opacity=0);
    border: 0;
    padding: 0;
    margin: 0;
	}
</style>
<!-- Modal -->
        <div class="modal fade" id="showMore" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                    <div class="row">
                        <div class="col-md-5 col-sm-6">
                            <div class="owl-carousel sync1">
                                <div class="item" id="pic-1"> <img id="pic-img-1" src="" alt=""> </div>
                                <div class="item" id="pic-2"> <img id="pic-img-2" src="" alt=""> </div>
                                <div class="item" id="pic-3"> <img id="pic-img-3" src="" alt=""> </div>
                                <div class="item" id="pic-4"> <img id="pic-img-4" src="" alt=""> </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-6">
                            <div class="product-single">
								<input type="hidden" name="base_url" value="<?php echo base_url(); ?>" id="base_url">
								<input type="hidden" name="item_id" id="item_id">
                                <div class="ps-header">
									<div id="view_auction_clock"></div>
                                    <h3 id="auc_name"></h3>
                                    <div class="ps-price">Min Price <b>Kr.</b> <strong id="auc_price"></strong></div>
									<span class="label label-success" id="ship_i" style="display:none">Ship International</span>&ensp;
									<span class="label label-primary" id="ship_l" style="display:none">Ship Local</span>&ensp;
									<span class="label label-warning"  id="pick_u" style="display:none">Pick Up</span>
                                </div>
                                <div class="sep"></div>
                                <div class="space10"></div>
								<em>Item Description</em>
								<p id="auc_desc"></p>
								<em id="auc_designer_label">Designer's Name</em>
								<p id="auc_designer"></p>
								<em>Location</em>
								<p id="auc_loc"></p>
								<div class="space30"></div>
								<?php if($this->session->userdata('loggedin') == TRUE){ ?>
									<div class="alert alert-success display-none" id="addBidSuccess">
										<p><strong>Success!</strong> Your bid is now added.</p>
									</div>

									<div class="alert alert-danger display-none" id="addBidError">
										<p><strong>Error!</strong> Error in adding your bid.</p>
									</div>
									<form id="send-bid-form"  method="post" data-toggle="validator" role="form">
										<ul class="form-list row">
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label >Bid Amount (Kr.)</label>
													<div class="help-block" id="lowBid" style="color: #a94442;display:none">Your bid must be greater than or equal to minimum price.</div>
													<input required type="number" step="0.01" name="bidder_price" id="bidder_price" class="form-control input-text">
													<input type="hidden" name="auction_id" id="auction_id">
													<input type="hidden" name="auction_min_price" id="auction_min_price">
													<div class="help-block with-errors"></div>
												</div>
											</li>
										</ul>
										<div class="buttons-set">
											<button class="btn-color" type="submit">SEND BID</button>
										</div>
									</form>
								<?php }else{ ?>
									<a href="<?php echo base_url(); ?>login" class="btn btn-primary">LOGIN TO BID</a>
								<?php } ?>
								<br/>
								<br/>
								<button style="padding: 4px 8px 2px 9px;" onclick="shareNow()" type="button" class="btn btn-primary text-center"><i style="font-size: 20px;" class="fa fa-facebook-square"></i> Post to my wall</button>
								<br/>
								<h5><u>Top Five Bids</u></h5>
								<table id="topFiveBid" class="table table-striped table-bordered fontArial" style="width:100%;display:none">
									<thead>
										<tr>
											<th>Member ID</th>
											<th>Bid Price (Kr.)</th>
										</tr>
									</thead>
									<tbody id="tbodyTopFiveBid">
									</tbody>
								</table>
						
								<div class="alert alert-info" id="notopfivebid" role="alert" style="display:none">
									<h4>No bidder/s.</h4>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
            <!-- BLOCKS -->
            <div class="block-main container no-padding-top">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <div class="block-content space30">
							<a href="<?php echo base_url();?>auction/categories/watches-and-clocks">
								<img src="<?php echo base_url();?>images/watches.jpg" class="img-responsive" alt=""/>
								<div class="text-style1">
									<h6 class="shadow2">Luxury<br>Watches</h6>
								</div>
							</a>
                        </div>
                        <div class="block-content space30">
							<a href="<?php echo base_url();?>auction/categories/jewellery" style="background:transparent;    padding-left: 0;">
								<img src="<?php echo base_url();?>images/jewellery.jpg" class="img-responsive" alt=""/>
								<div class="text-style2">
									<h6 class="shadow2">Jewellery</h6>
									<a href="<?php echo base_url();?>auction/categories/jewellery">Bid Now</a>
								</div>
							</a>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="home-carousel">
							<?php if(count($random) > 0 ){?>
								<?php foreach($random as $row){?>
									<div>
										<img src="<?php echo site_url("images/auctions/" . $row->user_id . "/" . $row->main_pic);?>" class="img-responsive img-slide-w" alt=""/>
										<div class="c-text">
											<h4 class="shadow"><?php echo ucwords($row->item_name); ?></h4>
											<p class="shadow"><?php echo $row->item_desc; ?></p>
											<span class="product-price text-white shadow">Min Kr. <?php echo number_format($row->min_price,2); ?></span>
											<a href="#" onclick="showAuctionDetails(<?php echo (strtotime($row->date_expired) - strtotime(date("Y-m-d H:i:s"))) . "," . $row->u_id; ?>)" class="btn btn-primary btn-sm">Bid Now <i class="fa fa-caret-right"></i></a>
										</div>
									</div>
								<?php } ?>	
							<?php }?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="block-content space30">
							<a href="<?php echo base_url();?>auction/categories/furniture">
								<img src="<?php echo base_url();?>images/furnitures.jpg" class="img-responsive" alt=""/>
								<div class="text-style1">
									<h6 class="shadow2">Furnitures</h6>
								</div>
							</a>
                        </div>
                        <div class="block-content space30">
							<a href="<?php echo base_url();?>auction/categories/clothing-shoes-and-accessories" style="background:transparent;    padding-left: 0;">
								<img src="<?php echo base_url();?>images/clothings.jpg" class="img-responsive" alt=""/>
								<div class="text-style2">
									<h6 class="shadow2">Clothings</h6>
									<a href="<?php echo base_url();?>auction/categories/clothing-shoes-and-accessories">Bid Now</a>
								</div>
							</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- PRODUCTS -->
            <div class="clearfix space50"></div>
            <div class="featured-products">
                <div class="container">
                    <div class="row">
                        <div class="heading-sub text-center">
                            <h5><span>Recent Auctions</span></h5>
                        </div>
                        <div id="isotope" class="isotope">
							<?php if(count($recent) > 0 ){?>
								<?php foreach($recent as $row){?>
									<div class="isotope-item clothing">
										<div id="recent_clock<?php echo $row->u_id; ?>"></div>
										<button class="recent_count" onclick="showRecentCountdown(<?php echo (strtotime($row->date_expired) - strtotime(date("Y-m-d H:i:s"))) . "," . $row->u_id; ?>)"></button>
										<div class="product-item">
											<div class="item-thumb">
												<img src="<?php echo site_url("images/auctions/" . $row->user_id . "/" . $row->main_pic);?>" class="img-responsive img-w-h" alt=""/>
											</div>
											<div class="product-info">
												<h4 class="product-title"><a href="#"><?php echo ucwords($row->item_name); ?></a></h4>
												<span class="product-price">Min Kr. <?php echo number_format($row->min_price,2); ?></span>
												<button class="btn btn-xs btn-default" onclick="showCurrency(<?php echo $row->u_id; ?>)">Currency Conversion</button>
												<br/>
												<br/>
												<div id="currencies<?php echo $row->u_id; ?>" class="display-none">	
													<span class="product-price">Min $ 
													<?php
														$req_url = 'https://api.exchangerate-api.com/v4/latest/DKK';
														$response_json = file_get_contents($req_url);
														if(false !== $response_json) {
															try {
																$response_object = json_decode($response_json);
																$USD_price = round(($row->min_price * $response_object->rates->USD), 2);
																echo $USD_price;
															}
															catch(Exception $e) {
															}
														}
													?>
													</span>
													<span class="product-price">Min € 
													<?php
														$req_url = 'https://api.exchangerate-api.com/v4/latest/DKK';
														$response_json = file_get_contents($req_url);
														if(false !== $response_json) {
															try {
																$response_object = json_decode($response_json);
																$EUR_price = round(($row->min_price * $response_object->rates->EUR), 2);
																echo $EUR_price;
															}
															catch(Exception $e) {
															}
														}
													?>
													</span>
												</div>
												<a href="#" onclick="showAuctionDetails(<?php echo (strtotime($row->date_expired) - strtotime(date("Y-m-d H:i:s"))) . "," . $row->u_id; ?>)" class="btn btn-danger">View More</a>
											</div>
										</div>
									</div>
								<?php }?>	
							<?php }else{?>
									<div class="alert alert-info" role="alert">
										<h4>No auctions.</h4>
									</div>
							<?php }?>	
                        </div>
                    </div>
                </div>
            </div>

            <!-- PRODUCTS -->
            <div class="container padding40">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="heading-sub heading-sub2 text-center">
                            <h5><span>Featured Auctions</span></h5>
						</div>
                        <div class="product-carousel3">
							<?php if(count($featured) > 0 ){?>
								<?php foreach($featured as $row){?>
									<div class="pc-wrap">
										<div id="feature_clock<?php echo $row->u_id; ?>"></div>
										<button class="btn_count" onclick="showFeatureCountdown(<?php echo (strtotime($row->date_expired) - strtotime(date("Y-m-d H:i:s"))) . "," . $row->u_id; ?>)"></button>
										<div class="product-item">
											<div class="item-thumb">
												<img src="<?php echo site_url("images/auctions/" . $row->user_id . "/" . $row->main_pic);?>" class="img-responsive img-w-h" alt=""/>
											</div>
											<div class="product-info">
												<h4 class="product-title"><a href="#"><?php echo ucwords($row->item_name); ?></a></h4>
												<span class="product-price">Min Kr. <?php echo number_format($row->min_price,2); ?></span>
												<button class="btn btn-xs btn-default" onclick="showFeaCurrency(<?php echo $row->u_id; ?>)">Currency Conversion</button>
												<br/>
												<br/>
												<div id="feature_currencies<?php echo $row->u_id; ?>" class="display-none">	
													<span class="product-price">Min $ 
													<?php
														$req_url = 'https://api.exchangerate-api.com/v4/latest/DKK';
														$response_json = file_get_contents($req_url);
														if(false !== $response_json) {
															try {
																$response_object = json_decode($response_json);
																$USD_price = round(($row->min_price * $response_object->rates->USD), 2);
																echo $USD_price;
															}
															catch(Exception $e) {
															}
														}
													?>
													</span>
													<span class="product-price">Min € 
													<?php
														$req_url = 'https://api.exchangerate-api.com/v4/latest/DKK';
														$response_json = file_get_contents($req_url);
														if(false !== $response_json) {
															try {
																$response_object = json_decode($response_json);
																$EUR_price = round(($row->min_price * $response_object->rates->EUR), 2);
																echo $EUR_price;
															}
															catch(Exception $e) {
															}
														}
													?>
													</span>
												</div>
												<a href="#" onclick="showAuctionDetails(<?php echo (strtotime($row->date_expired) - strtotime(date("Y-m-d H:i:s"))) . "," . $row->u_id; ?>)" class="btn btn-danger">View More</a>
												
											</div>
										</div>
									</div>
								<?php }?>	
							<?php }else{?>
									<div class="alert alert-info" role="alert">
										<h4>No featured auctions.</h4>
									</div>
							<?php }?>	
                        </div>
                    </div>
                </div>
            </div>