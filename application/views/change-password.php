
            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Change Password</li>
                    </ul>
                </div>
            </div>


            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
                    <div class="row">
						<div class="alert alert-success display-none" id="updatePassSuccess">
							<p><strong>Success!</strong> Password successfully updated.</p>
						</div>
						<div class="alert alert-danger display-none" id="updatePassError">
							<p><strong>Error!</strong> Password unsuccessfully updated.</p>
						</div>
						<div class="alert alert-danger display-none" id="diffPassError">
							<p><strong>Error!</strong> Password and Confirm Password must be the same.</p>
						</div>
                        <div class="col-md-9 col-sm-8">
                            <!-- HTML -->
                            <div id="account-id">
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>Change Your Password</h4>                                                                 
                                <div class="account-form">
                                    <form id="update-password-form" method="post" data-toggle="validator" role="form">                                      
                                        <ul class="form-list row">
                                            <li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Password *</label>
													<input type="password" name="password" id="password" class="form-control input-text" required>
													<input type="hidden" name="id" id="id" value="<?php echo $this->session->userdata('id') ?>"  class="form-control input-text">
													<div class="help-block with-errors"></div>
												</div>
											</li>
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Confirm Password *</label>
													<input type="password" name="cpassword" id="cpassword" class="form-control input-text" required>
													<div class="help-block with-errors"></div>
												</div>
											</li>                                             
                                        </ul>
                                        <div class="buttons-set">
                                            <button class="btn-black" type="submit"><span><span>SAVE</span></span></button>
                                        </div>
                                    </form>
                                </div>                                 
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-4 checkout-steps">
                            <h6>My Account</h6>
                            <div>
								<?php if($this->session->userdata('role') == 2){ ?>
									<ul class="account-list">
										<li><a href="<?php echo base_url();?>admin"  target="_blank"> <i class="fa fa-edit"></i> Manage Users</a></li>  
									</ul> 
									<br/>
								<?php }?>
                                <ul class="account-list">
									<li><a href="<?php echo base_url();?>account"> <i class="fa fa-edit"></i> My Account</a></li>   
                                    <li><a href="<?php echo base_url();?>account/edit"> <i class="fa fa-edit"></i> Edit Account</a></li>                     
                                    <li class="active"><a href="<?php echo base_url();?>account/changepassword"> <i class="fa fa-edit"></i> Change Password</a></li>
                                    <li><a href="<?php echo base_url();?>auction/items-list"> <i class="fa fa-edit"></i> Auction Items</a></li>
									<li><a href="<?php echo base_url();?>auction/bought"> <i class="fa fa-edit"></i> Bought Auction Items</a></li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>
