<style>
	.form-list .selectboxit-container{margin:0;}
	#categorySelectBoxItContainer{display:none;}
	select{display:block !important;}
	.fontArial{font-family:Arial}
	.sold-btn{    font-size: 1.15rem;
    padding: 4px 10px;}
</style> 
            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Auction Items</li>
                    </ul>
                </div>
            </div>


            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            <!-- HTML -->
                            <div id="account-id">
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>Bought Auction Items</h4>                                                                 
									
									<?php if(count($items) >0 ){?>
										<table id="boughtItemsTable" class="table table-striped table-bordered responsive" style="width:100%;">
											<thead>
												<tr>
													<th scope="col text-center">Item Name</th>
													<th scope="col text-center">Picture</th>
													<th scope="col text-center">Description</th>
													<th scope="col text-center">Category</th>
													<th scope="col text-center">Bid Price</th>
													<th scope="col text-center">Date Sold</th>
												</tr>
											</thead>
											<tbody id="tbodyallMyAuctions">
												<?php foreach($items as $row){?>
													<tr>
														<td><?php echo $row['item_name']; ?></td>
														<td><img src="<?php echo site_url("images/auctions/" . $row['seller_id'] . "/" . $row['main_pic']); ?>" style="width: 100px;" class="img-reponsive" /></td>
														<td><?php echo $row['item_desc']; ?></td>
														<td>
															<?php 
																switch($row['category']){
																	case 1:
																		echo "Carpets, rugs and textiles";
																		break;
																	case 2:
																		echo "Clothing, shoes and accessories";
																		break;
																	case 3:
																		echo "Electronics";
																		break;
																	case 4:
																		echo "Furniture";
																		break;
																	case 5:
																		echo "Glass, porcelain and ceramics";
																		break;
																	case 6:
																		echo "Hobby and collectibles";
																		break;
																	case 7:
																		echo "Home and garden";
																		break;
																	case 8:
																		echo "Hunting, fishing, arms and militaria";
																		break;
																	case 9:
																		echo "Jewellery";
																		break;
																	case 10:
																		echo "Lamps and lighting";
																		break;
																	case 11:
																		echo "Paintings and sculptures";
																		break;
																	case 12:
																		echo "Silver, bronze, copper and pewter";
																		break;
																	case 13:
																		echo "Sport and leisure";
																		break;
																	case 14:
																		echo "Travel and experiences";
																		break;
																	case 15:
																		echo "Watches and clocks";
																		break;
																	case 16:
																		echo "Wine and spirits";
																		break;
																}
															?>
														</td>
														<td><?php echo $row['bid_price']; ?></td>

														<td><?php echo date("F d, Y", strtotime($row['date_sold'])); ?></td>
													</tr>
												<?php } ?>								
											</tbody>
										</table>
									<?php }else{?>
									<div class="alert alert-info" role="alert">
										<h4>No data to be displayed.</h4>
									</div>
									<?php }?>
                                                                
                            </div>
                        
					   </div>
						
                        <div class="col-md-3 col-sm-4 checkout-steps">
                            <h6>My Account</h6>
                            <div>
								<?php if($this->session->userdata('role') == 2){ ?>
									<ul class="account-list">
										<li><a href="<?php echo base_url();?>admin" target="_blank"> <i class="fa fa-edit"></i> Manage Users</a></li>  
									</ul> 
									<br/>
								<?php }?>
                                <ul class="account-list">
									<li><a href="<?php echo base_url();?>account"> <i class="fa fa-edit"></i> My Account</a></li>   
                                    <li><a href="<?php echo base_url();?>account/edit"> <i class="fa fa-edit"></i> Edit Account</a></li>                     
                                    <li><a href="<?php echo base_url();?>account/changepassword"> <i class="fa fa-edit"></i> Change Password</a></li>
                                    <li><a href="<?php echo base_url();?>auction/items-list"> <i class="fa fa-edit"></i> Auction Items</a></li>
                                    <li class="active"><a href="<?php echo base_url();?>auction/bought"> <i class="fa fa-edit"></i> Bought Auction Items</a></li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>
		<div class="modal fade" id="updateAuctionModal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3><span class="fa fa-chevron-right"></span> Edit Auction Item Details</h3>
						<div>
							<form id="update-auction-form" method="post" data-toggle="validator" role="form" enctype="multipart/form-data">
								<div class="text-center">
									<ul class="form-list row" style="display: inline-block;">
										
											<li class="col-md-3 col-sm-3 col-xs-6" id="pic-1" style="width: auto;display:none">
												<p>Main Picture</p> <img id="pic-img-1" style="height: 120px;" class="img-reponsive" /></li>

													<li class="col-md-3 col-sm-3 col-xs-6" id="pic-2" style="width: auto;;display:none">
														<p>Other Picture 1</p> <img id="pic-img-2" style="height: 120px;" class="img-reponsive" /></li>
	
															<li class="col-md-3 col-sm-3 col-xs-6" id="pic-3" style="width: auto;;display:none">
																<p>Other Picture 2</p> <img id="pic-img-3" style="height: 120px;" class="img-reponsive" /></li>

																	<li class="col-md-3 col-sm-3 col-xs-6" id="pic-4" style="width: auto;;display:none">
																		<p>Other Picture 3</p> <img  id="pic-img-4"style="height: 120px;" class="img-reponsive" /></li>
									</ul>
								</div>
								<ul class="form-list row">
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Auction Name</label>
											<input required type="text" name="item_name" id="item_name" class="form-control input-text">
											<input type="hidden" name="base_url" value="<?php echo base_url(); ?>" id="base_url">
											<input type="hidden" name="item_id" id="item_id">
											<input type="hidden" name="old_main_pic" id="old_main_pic">
											<input type="hidden" name="old_other_pic1" id="old_other_pic1">
											<input type="hidden" name="old_other_pic2" id="old_other_pic2">
											<input type="hidden" name="old_other_pic3" id="old_other_pic3">
											<div class="help-block with-errors"></div>
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Main Picture</label>
											<input type="file" class="form-control input-text" name="main_pic" id="main_pic">
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Minimum Price</label>
											<input type="text" class="form-control input-text" name="min_price" id="min_price" required>
											<div class="help-block with-errors"></div>
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Other Picture 1</label>
													<input type="file" class="form-control input-text" name="other_pic1" id="other_pic1">
												</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Designer's Name (Optional)</label>
											<input type="text" name="designer_name"  id="designer_name" class="form-control input-text">
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Other Picture 2</label>
											<input type="file" class="form-control input-text" name="other_pic2" id="other_pic2">
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Category</label>
													<select name="category" class="form-control" id="category" required>
														<option value="1">Carpets, rugs and textiles</option>
														<option value="2">Clothing, shoes and accessories</option>
														<option value="3">Electronics</option>
														<option value="4">Furniture</option>
														<option value="5">Glass, porcelain and ceramics</option>
														<option value="6">Hobby and collectibles</option>
														<option value="7">Home and garden</option>
														<option value="8">Hunting, fishing, arms and militaria</option>
														<option value="9">Jewellery</option>
														<option value="10">Lamps and lighting</option>
														<option value="11">Paintings and sculptures</option>
														<option value="12">Silver, bronze, copper and pewter</option>
														<option value="13">Sport and leisure</option>
														<option value="14">Travel and experiences</option>
														<option value="15">Watches and clocks</option>
														<option value="16">Wine and spirits</option>
													</select>
											<div class="help-block with-errors"></div>
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Other Picture 3</label>
													<input type="file" class="form-control input-text" name="other_pic3" id="other_pic3">
												</div>
											</li>
									<li class="col-md-12 col-sm-12">
										<div class="form-group">
											<label>Description</label>
											<textarea type="text" name="item_desc" rows="3" id="item_desc" class="form-control input-text" required>
											</textarea>
											<div class="help-block with-errors"></div>
										</div>
									</li>
								</ul>
								<div class="buttons-set text-right">
									<button class="btn-black" type="submit"><span><span>SAVE</span></span>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>