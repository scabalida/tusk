<style>
.item-thumb {
    height: 235px;
    width: 100%;
	background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.flip-clock-wrapper ul li a div div.inn {
				    background-color: transparent;
    background-repeat: no-repeat;
    background-image: url('images/jewel.jpg');
    background-position: bottom;
    background-size: cover;
			}
.btn_count{
		    opacity: 0;
    filter: alpha(opacity=0);
    border: 0;
    padding: 0;
    margin: 0;
	}
</style>
<?php 
	$currentURL = current_url();
	$chckURL = strpos($currentURL, "categories");
	if($chckURL == 23){
?>
<style>
	.flip-clock-wrapper ul li a div div.inn {
		background-image: url('../../images/jewel.jpg');
	}
</style>
<?php } ?>
       <!-- Modal -->
        <div class="modal fade" id="showMore" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                    <div class="row">
                        <div class="col-md-5 col-sm-6">
                            <div class="owl-carousel sync1">
                                <div class="item" id="pic-1"> <img id="pic-img-1" src="" alt=""> </div>
                                <div class="item" id="pic-2"> <img id="pic-img-2" src="" alt=""> </div>
                                <div class="item" id="pic-3"> <img id="pic-img-3" src="" alt=""> </div>
                                <div class="item" id="pic-4"> <img id="pic-img-4" src="" alt=""> </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-6">
                            <div class="product-single">
								<input type="hidden" name="base_url" value="<?php echo base_url(); ?>" id="base_url">
								<input type="hidden" name="item_id" id="item_id">
                                <div class="ps-header">
									<div id="view_auction_clock"></div>
                                    <h3 id="auc_name"></h3>
                                    <div class="ps-price">Min Price <b>Kr.</b> <strong id="auc_price"></strong></div>
									<span class="label label-success" id="ship_i" style="display:none">Ship International</span>&ensp;
									<span class="label label-primary" id="ship_l" style="display:none">Ship Local</span>&ensp;
									<span class="label label-warning"  id="pick_u" style="display:none">Pick Up</span>
								</div>
                                <div class="sep"></div>
                                <div class="space10"></div>
								<em>Item Description</em>
								<p id="auc_desc"></p>
								<em id="auc_designer_label">Designer's Name</em>
								<p id="auc_designer"></p>
								<em>Location</em>
								<p id="auc_loc"></p>
								<div class="space30"></div>
								<?php if($this->session->userdata('loggedin') == TRUE){ ?>
									<div class="alert alert-success display-none" id="addBidSuccess">
										<p><strong>Success!</strong> Your bid is now added.</p>
									</div>

									<div class="alert alert-danger display-none" id="addBidError">
										<p><strong>Error!</strong> Error in adding your bid.</p>
									</div>
									<form id="send-bid-form"  method="post" data-toggle="validator" role="form">
										<ul class="form-list row">
											<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label >Bid Amount (Kr.)</label>
													<div class="help-block" id="lowBid" style="color: #a94442;display:none">Your bid must be greater than or equal to minimum price.</div>
													<input required type="number" step="0.01" name="bidder_price" id="bidder_price" class="form-control input-text">
													<input type="hidden" name="auction_id" id="auction_id">
													<input type="hidden" name="auction_min_price" id="auction_min_price">
													<div class="help-block with-errors"></div>
												</div>
											</li>
										</ul>
										<div class="buttons-set">
											<button class="btn-color" type="submit">SEND BID</button>
										</div>
									</form>
								<?php }else{ ?>
									<a href="<?php echo base_url(); ?>login" class="btn btn-primary">LOGIN TO BID</a>
								<?php } ?>
								<br/>
								<br/>
								<button style="padding: 4px 8px 2px 9px;" onclick="shareNow()" type="button" class="btn btn-primary text-center"><i style="font-size: 20px;" class="fa fa-facebook-square"></i> Post to my wall</button>
								<br/>
								<h5><u>Top Five Bids</u></h5>
								<table id="topFiveBid" class="table table-striped table-bordered fontArial" style="width:100%;display:none">
									<thead>
										<tr>
											<th>Member ID</th>
											<th>Bid Price (Kr.)</th>
										</tr>
									</thead>
									<tbody id="tbodyTopFiveBid">
									</tbody>
								</table>
						
								<div class="alert alert-info" id="notopfivebid" role="alert" style="display:none">
									<h4>No bidder/s.</h4>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
			<!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Auction Items</li>
                    </ul>
                </div>
            </div>

            <!-- SHOP CONTENT -->
            <div class="shop-content">
                <div class="container">
                    <div class="row">
                        <aside class="col-md-3 col-sm-4">
                            <!--<div class="side-widget space50">
                                 <form role="form" class="search-widget">
                                    <input class="form-control" type="text">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>-->
                            <div class="side-widget">
                                <h5>Categories</h5>
                                <ul class="cat-list">
                                    <li><a href="<?php echo base_url(); ?>auction/categories/carpets-rugs-and-textiles">Carpets, rugs and textiles</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/clothing-shoes-and-accessories">Clothing, shoes and accessories</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/electronics">Electronics</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/furniture">Furniture</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/glass-porcelain-and-ceramics">Glass, porcelain and ceramics</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/hobby-and-collectibles">Hobby and collectibles</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/home-and-garden">Home and garden</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/hunting-fishing-arms-and-militaria">Hunting, fishing, arms and militaria</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/jewellery">Jewellery</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/lamps-and-lighting">Lamps and lighting</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/paintings-and-sculptures">Paintings and sculptures</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/silver-bronze-copper-and-pewter">Silver, bronze, copper and pewter</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/sport-and-leisure">Sport and leisure</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/travel-and-experiences">Travel and experiences</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/watches-and-clocks">Watches and clocks</a></li>
                                    <li><a href="<?php echo base_url(); ?>auction/categories/wine-and-spirits">Wine and spirits</a></li>
                                </ul>
                                <div class="clearfix space20"></div>
                            </div>
                        </aside>
                        <div class="col-md-9 col-sm-8">
                            <!--<div class="filter-wrap">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        View as: <span><a class="active">Grid</a> / <a href="./categories-list.html">List</a></span>
                                    </div>
                                    <div class="col-md-5 col-sm-5">
                                        Sort by:
                                        <select>
                                            <option>Default</option>
                                            <option>Newest</option>
                                            <option>Popular</option>
                                            <option>Recently Sold</option>
                                        </select>
                                    </div>
                                </div>
                            </div>-->
							<?php if(count($items) >0 ){?>
                            <div class="row">
								<?php foreach($items as $row){?>
									<div class="col-md-4 col-sm-6">
										<div id="btn_count<?php echo $row->u_id; ?>"></div>
										<button class="btn_count" onclick="showCountdown(<?php echo (strtotime($row->date_expired) - strtotime(date("Y-m-d H:i:s"))) . "," . $row->u_id; ?>)"></button>
										<div class="product-item">
											
											<?php echo '<div class="item-thumb" style="background-image:url('. site_url("images/auctions/" .$row->user_id. "/" .$row->main_pic).')"></div>'; ?>
											<div class="product-info">
												<h4 class="product-title"><a href="#"><?php echo ucwords($row->item_name); ?></a></h4>
												<span class="product-price">Min Kr. <?php echo number_format($row->min_price,2); ?></span>
												<button class="btn btn-xs btn-default" onclick="showCurrency(<?php echo $row->u_id; ?>)">Currency Conversion</button>
												<br/>
												<br/>
												<div id="currencies<?php echo $row->u_id; ?>" class="display-none">	
													<span class="product-price">Min $ 
													<?php
														$req_url = 'https://api.exchangerate-api.com/v4/latest/DKK';
														$response_json = file_get_contents($req_url);
														if(false !== $response_json) {
															try {
																$response_object = json_decode($response_json);
																$USD_price = round(($row->min_price * $response_object->rates->USD), 2);
																echo $USD_price;
															}
															catch(Exception $e) {
															}
														}
													?>
													</span>
													<span class="product-price">Min € 
													<?php
														$req_url = 'https://api.exchangerate-api.com/v4/latest/DKK';
														$response_json = file_get_contents($req_url);
														if(false !== $response_json) {
															try {
																$response_object = json_decode($response_json);
																$EUR_price = round(($row->min_price * $response_object->rates->EUR), 2);
																echo $EUR_price;
															}
															catch(Exception $e) {
															}
														}
													?>
													</span>
												</div>
												<a href="#" onclick="showAuctionDetails(<?php echo (strtotime($row->date_expired) - strtotime(date("Y-m-d H:i:s"))) . "," . $row->u_id; ?>)" class="btn btn-danger">View More</a>
											</div>
										</div>
									</div>
								<?php }?>
                            </div>
							<?php }else{?>
									<div class="alert alert-info" role="alert">
										<h4>No data to be displayed.</h4>
									</div>
							<?php }?>
						</div>
                    </div>
                </div>
            </div>

            <div class="clearfix space20"></div>
			<script>
				function showCurrency(id){
					//console.log(id);
					$("#currencies" + id).toggle();
				}
			</script>