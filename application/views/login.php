<!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Login</li>
                    </ul>
                </div>
            </div>
            <div class="space10"></div>

			
            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <!-- HTML -->
                            <div id="account-id">
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>Login</h4>                                                                  
                                <div class="account-form">
                                    <form id="login-form" class="form-login" method="post" data-toggle="validator" role="form">                                        
                                        <div class="alert alert-danger display-none" id="notPaid">
											<p><strong>Error!</strong> Please pay your membership fee.</p>
										</div>
										<div class="alert alert-danger display-none" id="accountExpired">
											<p><strong>Error!</strong> Your account has been expired. Please renew it now. <a href="<?php echo base_url();?>account/renew" target="_blank">Renew now!</a></p>
										</div>
										<div class="alert alert-danger display-none" id="loginInvalid">
											<p><strong>Error!</strong> Invalid Username/Password.</p>
										</div>
										<ul class="form-list row">
                                            <li class="col-md-12 col-sm-12">
												<div class="form-group">
													<label >Email <em>*</em></label>
													<input required type="text" class="input-text form-control" name="email" id="email">
													<div class="help-block with-errors"></div>
												</div>
											</li>
                                            <li class="col-md-12 col-sm-12">
												<div class="form-group">
													<label >Your password <em>*</em></label>
													<input required type="password" class="input-text form-control" name="pass" id="pass">
													<div class="help-block with-errors"></div>
												</div>
											</li> 
                                        </ul>
                                        <div class="buttons-set">
                                            <button class="btn-black" type="submit"><span>login</span></button>
                                        </div>
                                    </form>
                                </div>                                    
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6">
                            <!-- HTML -->
                            <div id="account-id2">
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>Create New Account</h4>                                                                  
                                <div class="account-form create-new-account">
                                    <h3 class="block-title">Signup Today and You'll be able to</h3>
                                    <ul>
                                        <li> <i class="fa fa-edit"></i> Online Order Status</li>
                                        <li> <i class="fa fa-edit"></i> See Ready Hot Deals</li>
                                        <li> <i class="fa fa-edit"></i> Love List</li>
                                        <li> <i class="fa fa-edit"></i> Quick Buy Stuffs</li>
                                    </ul>
                                    <div class="buttons-set">
                                        <a class="btn-black" href="<?php echo base_url(); ?>account/register"><span>Create Account</span></a>
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>