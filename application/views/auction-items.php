<style>
	.form-list .selectboxit-container{margin:0;}
	#categorySelectBoxItContainer{display:none;}
	select{display:block !important;}
	.fontArial{font-family:Arial}
	.sold-btn{    font-size: 1.15rem;
    padding: 4px 10px;}
</style>
       <!-- Modal -->
	    <div class="modal fade" id="displayBuyerInfo" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
						<h4 class="modal-title">Buyer Info</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<p>Full Name</p>
								<h4 id="showBuyerName"></h4>
								<p>Home Address</p>
								<h4 id="showBuyerAddress"></h4>
								<p>Country</p>
								<h4 id="showBuyerCountry"></h4>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<p>Email Address</p>
								<h4 id="showBuyerEmail"></h4>
								<p>Zip</p>
								<h4 id="showBuyerZip"></h4>
								<p>Contact Number</p>
								<h4 id="showBuyerContact"></h4>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div> 
        <div class="modal fade" id="displayBids" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
					<h4 class="modal-title">Bidders</h4>
					<div class="modal-body">
						
							<table id="myBiddersTable" class="table table-striped table-bordered fontArial" style="width:100%;display:none">
								<thead style="display:none">
									<tr>
										<th scope="col text-center"></th>
									</tr>
								</thead>
								<tbody id="tbodyMyBidders">
								</tbody>
							</table>
						
						<div class="alert alert-info" id="nobidders" role="alert" style="display:none">
							<h4>No bidder/s.</h4>
						</div>
					</div>
				</div>
            </div>
        </div> 
						<div class="modal fade" id="confirmSoldItem" tabindex="-1" role="dialog">
								<div class="modal-dialog">
																<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>

											<div class="text-center">
												<h3 class="agileinfo_sign">Are you sure?</h3>
													<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
													<button type="button" id="soldNow" class="btn btn-danger">YES</button>
											</div>
										</div>
									</div>
								</div>
						</div>
						
            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Auction Items</li>
                    </ul>
                </div>
            </div>


            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
                    <div class="row">
						<div class="alert alert-success display-none" id="updateAuctionItemSuccess">
							<p><strong>Success!</strong> Auction item successfully updated.</p>
						</div>
						<div class="alert alert-danger display-none" id="updateAuctionItemError">
							<p><strong>Error!</strong> Auction item unsuccessfully updated.</p>
						</div>
						<div class="alert alert-success display-none" id="deleteAuctionItemSuccess">
							<p><strong>Success!</strong> Auction item successfully deleted.</p>
						</div>
						<div class="alert alert-danger display-none" id="deleteAuctionItemError">
							<p><strong>Error!</strong> Auction item unsuccessfully deleted.</p>
						</div>
						
						<div class="alert alert-success display-none" id="soldAuctionItemSuccess">
							<p><strong>Success!</strong> Auction item successfully sold.</p>
						</div>
						<div class="alert alert-danger display-none" id="soldAuctionItemError">
							<p><strong>Error!</strong> Auction item unsuccessfully sold.</p>
						</div>
						
						<div class="alert alert-success display-none" id="renewAuctionItemSuccess">
							<p><strong>Success!</strong> Auction item successfully renewed.</p>
						</div>
						<div class="alert alert-danger display-none" id="renewAuctionItemError">
							<p><strong>Error!</strong> Auction item unsuccessfully renewed.</p>
						</div>
						
						<div class="alert alert-success display-none" id="imageDeleteItemSuccess">
							<p><strong>Success!</strong> Auction item picture successfully deleted.</p>
						</div>
						<div class="alert alert-danger display-none" id="imageDeleteItemError">
							<p><strong>Error!</strong> Auction item picture unsuccessfully deleted.</p>
						</div>
                        <div class="col-md-9 col-sm-8">
                            <!-- HTML -->
                            <div id="account-id">
								<div class="text-right"><a href="<?php echo base_url();?>auction/add" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i> Add Item</a></div>
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>Manage Auction Items</h4>                                                                 
									
									<?php if(count($items) >0 ){?>
										<table id="allMyAuctionsTable" class="table table-striped table-bordered responsive" style="width:100%;">
											<thead>
												<tr>
													<th scope="col text-center">Auction Item ID</th>
													<th scope="col text-center">Name</th>
													<th scope="col text-center">Picture</th>
													<!--<th scope="col text-center">Category</th>-->
													<th scope="col" class="text-center">Status</th>
													<th scope="col text-center">Date Expired</th>
													<th scope="col" class="text-center">Featured This Item?</th>
													<th scope="col" class="text-center">Actions</th>
												</tr>
											</thead>
											<tbody id="tbodyallMyAuctions">
												<?php foreach($items as $row){?>
													<div class="modal fade" id="deleteAuctionModal<?php echo $row->u_id; ?>" tabindex="-1" role="dialog">
														<div class="modal-dialog">
															<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>

																	<div class="text-center">
																		<h3 class="agileinfo_sign">REMOVE THIS ITEM?</h3>
																		<form onsubmit="return delete_auction_form(<?php echo $row->u_id; ?>);" method="post">
																			<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
																			<button type="submit" class="btn btn-danger">DELETE</button>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal fade" id="renewAuctionInstantModal<?php echo $row->u_id; ?>" tabindex="-1" role="dialog">
														<div class="modal-dialog">
															<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>

																	<div class="text-center">
																		<h3 class="agileinfo_sign">RENEW THIS ITEM?</h3>
																		<form onsubmit="return renew_instant(<?php echo $row->u_id; ?>);" method="post">
																			<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
																			<button type="submit" class="btn btn-danger">YES</button>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal fade" id="renewAuctionModal<?php echo $row->u_id; ?>" tabindex="-1" role="dialog">
														<div class="modal-dialog">
															<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>

																	<div class="text-center">
																		<h3 class="agileinfo_sign">RENEW THIS ITEM?</h3>
																		<form onsubmit="return renew_auction_form(<?php echo $row->u_id; ?>);" method="post">
																			<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
																			<button type="submit" class="btn btn-danger">YES (Kr. 30)</button>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<tr>
														<td><?php echo $row->u_id; ?></td>
														<td><?php echo $row->item_name; ?></td>
														<td><img src="<?php echo site_url("images/auctions/" . $this->session->userdata('id') . "/" . $row->main_pic); ?>" style="width: 100px;" class="img-reponsive" /></td>
														<!--<td>
															<?php 
																switch($row->category){
																	case 1:
																		echo "Carpets, rugs and textiles";
																		break;
																	case 2:
																		echo "Clothing, shoes and accessories";
																		break;
																	case 3:
																		echo "Electronics";
																		break;
																	case 4:
																		echo "Furniture";
																		break;
																	case 5:
																		echo "Glass, porcelain and ceramics";
																		break;
																	case 6:
																		echo "Hobby and collectibles";
																		break;
																	case 7:
																		echo "Home and garden";
																		break;
																	case 8:
																		echo "Hunting, fishing, arms and militaria";
																		break;
																	case 9:
																		echo "Jewellery";
																		break;
																	case 10:
																		echo "Lamps and lighting";
																		break;
																	case 11:
																		echo "Paintings and sculptures";
																		break;
																	case 12:
																		echo "Silver, bronze, copper and pewter";
																		break;
																	case 13:
																		echo "Sport and leisure";
																		break;
																	case 14:
																		echo "Travel and experiences";
																		break;
																	case 15:
																		echo "Watches and clocks";
																		break;
																	case 16:
																		echo "Wine and spirits";
																		break;
																}
															?>
														</td>-->
														<td class="text-center">
														<?php if(strtotime(date("Y-m-d H:i:s")) > strtotime(date("Y-m-d H:i:s" , strtotime($row->date_expired)))){ ?>
															<?php if($row->status == 2){ ?>
																<p class="label label-success text-center">SOLD</p>		
															<?php }else{ ?>
																<p class="label label-danger text-center">Expired</p>	
															<?php } ?>
														<?php }else{ ?>
															<?php if($row->status == 1){ ?>
																<p class="label label-primary text-center">ON AUCTION</p>		
															<?php }if($row->status == 2){ ?>
																<p class="label label-success text-center">SOLD</p>		
															<?php } ?>
														<?php } ?>
														</td>
														<td><?php echo date("F d, Y", strtotime($row->date_expired)); ?></td>
														<td class="text-center">
															<?php if($row->featured == 1){ ?>
																<p class="label label-success text-center">FEATURED</p>		
															<?php }else{ ?>
																<button class="btn btn-sm btn-info" onclick="featuredThis(<?php echo $row->u_id; ?>)">Yes (Kr. 20)</button>
															<?php } ?>
														</td>
														<td>
															<div class="dropdown">
															  <a class="btn btn-primary dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-cog"></i>
															  <span class="caret"></span></a>
															  <ul class="dropdown-menu" style="right:0;left: unset;">
																<?php if($row->status == 2){ ?>
																	<li><a href="#" onclick="showBuyerInfo(<?php echo $row->u_id; ?>)" style=" color: #337ab7;"><i class="fa fa-eye"></i> Show Buyer</a></li>
																<?php } ?>
																<?php if((strtotime(date("Y-m-d H:i:s")) > strtotime(date("Y-m-d H:i:s" , strtotime($row->date_expired)))) && $row->status != 2){ ?>
																	<?php if($this->session->userdata('email') == "matras@live.dk" || $this->session->userdata('email') == "cabalida.stephen@gmail.com"){ ?>
																		<li><a href="#" data-toggle="modal" data-target="#renewAuctionInstantModal<?php echo $row->u_id; ?>" style=" color: #337ab7;"><i class="fa fa-arrow-circle-up"></i> Renew</a></li>
																	<?php }else{?>
																		<li><a href="#" data-toggle="modal" data-target="#renewAuctionModal<?php echo $row->u_id; ?>" style=" color: #337ab7;"><i class="fa fa-arrow-circle-up"></i> Renew</a></li>
																	<?php } ?>
																<?php } ?>
																<?php if($row->status == 0 || $row->status == 1){ ?>
																	<li><a href="#" onclick="showBidders(<?php echo $row->u_id; ?>)" style=" color: #337ab7;"><i class="fa fa-eye"></i> Show Bids</a></li>
																<?php } ?>
																	<li><a href="#" onclick="showUpdateAuction(<?php echo $row->u_id; ?>)" style=" color: #3c763d;"><i class="fa fa-pencil"></i> Edit</a></li>
																	<li><a href="#" data-toggle="modal" data-target="#deleteAuctionModal<?php echo $row->u_id; ?>" style=" color: #a94442;"><i class="fa fa-trash"></i> Remove</a></li>
															  </ul>
															</div>
														</td>
													</tr>
												<?php } ?>								
											</tbody>
										</table>
									<?php }else{?>
									<div class="alert alert-info" role="alert">
										<h4>No data to be displayed.</h4>
									</div>
									<?php }?>
                                                                
                            </div>
                        
					   </div>
						
                        <div class="col-md-3 col-sm-4 checkout-steps">
                            <h6>My Account</h6>
                            <div>
								<?php if($this->session->userdata('role') == 2){ ?>
									<ul class="account-list">
										<li><a href="<?php echo base_url();?>admin" target="_blank"> <i class="fa fa-edit"></i> Manage Users</a></li>  
									</ul> 
									<br/>
								<?php }?>
                                <ul class="account-list">
									<li><a href="<?php echo base_url();?>account"> <i class="fa fa-edit"></i> My Account</a></li>   
                                    <li><a href="<?php echo base_url();?>account/edit"> <i class="fa fa-edit"></i> Edit Account</a></li>                     
                                    <li><a href="<?php echo base_url();?>account/changepassword"> <i class="fa fa-edit"></i> Change Password</a></li>
                                    <li class="active"><a href="<?php echo base_url();?>auction/items-list"> <i class="fa fa-edit"></i> Auction Items</a></li>
									<li><a href="<?php echo base_url();?>auction/bought"> <i class="fa fa-edit"></i> Bought Auction Items</a></li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>
		<div class="modal fade" id="updateAuctionModal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3><span class="fa fa-chevron-right"></span> Edit Auction Item Details</h3>
						<div>
							<form id="update-auction-form" method="post" data-toggle="validator" role="form" enctype="multipart/form-data">
								<div class="text-center">
									<ul class="form-list row" style="display: inline-block;">
										<li class="col-md-3 col-sm-3 col-xs-6" id="pic-1" style="width: auto;display:none">
											<p>Main Picture</p> <img id="pic-img-1" style="height: 120px;" class="img-reponsive" />

										</li>
										<li class="col-md-3 col-sm-3 col-xs-6" id="pic-2" style="width: auto;display:none">
											<p>Other Picture 1</p> <img id="pic-img-2" style="height: 120px;" class="img-reponsive" />
										</li>
										<li class="col-md-3 col-sm-3 col-xs-6" id="pic-3" style="width: auto;display:none">
											<p>Other Picture 2</p> <img id="pic-img-3" style="height: 120px;" class="img-reponsive" />
											<div>
												<br/>
												<button class="btn btn-danger" id="deletePic3" data-id="" data-name="" data-db="" >Remove</button>
											</div>
										</li>
										<li class="col-md-3 col-sm-3 col-xs-6" id="pic-4" style="width: auto;display:none">
											<p>Other Picture 3</p> <img  id="pic-img-4"style="height: 120px;" class="img-reponsive" />
											<div>
												<br/>
												<button class="btn btn-danger" id="deletePic4" data-id="" data-name="" data-db="" >Remove</button>
											</div>
										</li>
									</ul>
								</div>
								<br/>
								<ul class="form-list row">
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Auction Name</label>
											<input required type="text" name="item_name" id="item_name" class="form-control input-text">
											<input type="hidden" name="base_url" value="<?php echo base_url(); ?>" id="base_url">
											<input type="hidden" name="item_id" id="item_id">
											<input type="hidden" name="old_main_pic" id="old_main_pic">
											<input type="hidden" name="old_other_pic1" id="old_other_pic1">
											<input type="hidden" name="old_other_pic2" id="old_other_pic2">
											<input type="hidden" name="old_other_pic3" id="old_other_pic3">
											<div class="help-block with-errors"></div>
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Main Picture</label>
											<input type="file" class="form-control input-text" name="main_pic" id="main_pic">
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Minimum Price</label>
											<input type="text" class="form-control input-text" name="min_price" id="min_price" required>
											<div class="help-block with-errors"></div>
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Other Picture 1</label>
													<input type="file" class="form-control input-text" name="other_pic1" id="other_pic1">
												</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Designer's Name (Optional)</label>
											<input type="text" name="designer_name"  id="designer_name" class="form-control input-text">
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Other Picture 2</label>
											<input type="file" class="form-control input-text" name="other_pic2" id="other_pic2">
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
										<div class="form-group">
											<label>Category</label>
													<select name="category" class="form-control" id="category" required>
														<option value="1">Carpets, rugs and textiles</option>
														<option value="2">Clothing, shoes and accessories</option>
														<option value="3">Electronics</option>
														<option value="4">Furniture</option>
														<option value="5">Glass, porcelain and ceramics</option>
														<option value="6">Hobby and collectibles</option>
														<option value="7">Home and garden</option>
														<option value="8">Hunting, fishing, arms and militaria</option>
														<option value="9">Jewellery</option>
														<option value="10">Lamps and lighting</option>
														<option value="11">Paintings and sculptures</option>
														<option value="12">Silver, bronze, copper and pewter</option>
														<option value="13">Sport and leisure</option>
														<option value="14">Travel and experiences</option>
														<option value="15">Watches and clocks</option>
														<option value="16">Wine and spirits</option>
													</select>
											<div class="help-block with-errors"></div>
										</div>
									</li>
									<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Other Picture 3</label>
													<input type="file" class="form-control input-text" name="other_pic3" id="other_pic3">
												</div>
											</li>
									<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<label>Location</label>
													<input required type="text" name="item_loc" id="item_loc" class="form-control input-text">
													<div class="help-block with-errors"></div>
												</div>
											</li>
									<li class="col-md-6 col-sm-6">
												<div class="form-group">
													<p><strong>Shipment</strong></p>
													<label class="checkbox-inline"><input type="checkbox" id="ship_to_international_u" name="ship_to_international" >International</label>
													<label class="checkbox-inline"><input type="checkbox" id="ship_to_local_u" name="ship_to_local" >Local</label>
													<label class="checkbox-inline"><input type="checkbox" id="pick_up_u" name="pick_up" >Pick up</label>
												</div>
											</li>
									<li class="col-md-12 col-sm-12">
										<div class="form-group">
											<label>Description</label>
											<textarea type="text" name="item_desc" rows="3" id="item_desc" class="form-control input-text" required>
											</textarea>
											<div class="help-block with-errors"></div>
										</div>
									</li>
								</ul>
								<div class="buttons-set text-right">
									<button class="btn-black" type="submit"><span><span>SAVE</span></span>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="deletePicModal" tabindex="-1" role="dialog">
								<div class="modal-dialog">
																<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>

											<div class="text-center">
												<h3 class="agileinfo_sign">Are you sure you want to delete?</h3>
													<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
													<button type="button" id="deletePic" class="btn btn-danger">YES</button>
											</div>
										</div>
									</div>
								</div>
							</div>
		<div class="modal fade" id="featuredItemModal" tabindex="-1" role="dialog">
								<div class="modal-dialog">
																<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>

											<div class="text-center">
												<h3 class="agileinfo_sign">Are you sure you want to feature this?</h3>
													<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
													<button type="button" id="featuredNow" class="btn btn-danger">YES</button>
											</div>
										</div>
									</div>
								</div>
							</div>