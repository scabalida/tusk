<!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Renew Account</li>
                    </ul>
                </div>
            </div>
            <div class="space10"></div>

			
            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-md-3 col-xs-12"></div>
                        <div class="col-sm-6 col-md-6 col-xs-12">
                            <!-- HTML -->
                            <div id="account-id">
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>Renew Account</h4>                                                                  
                                <div class="account-form">
                                    <form action="<?php echo base_url();?>account/renewAccount2" method="post" >                                        
										<ul class="form-list row">
                                            <li class="col-md-12 col-sm-12">
												<div class="form-group">
													<label >Email <em>*</em></label>
													<input required type="text" class="input-text form-control" name="email" id="email">
												</div>
											</li>
                                        </ul>
                                        <div class="buttons-set">
                                            <button class="btn-black" type="submit"><span>Renew</span></button>
                                        </div>
                                    </form>
                                </div>                                    
                            </div>
                        </div>
						<div class="col-sm-3 col-md-3 col-xs-12"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>