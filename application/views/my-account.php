
            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>My Account</li>
                    </ul>
                </div>
            </div>

			
            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
					<p><a href="<?php echo base_url(); ?>account/renewAccount" class="btn btn-primary" target="_blank">Renew Account</a></p>
                    <div class="row">
                        <div class="col-md-5 col-sm-4">
                            <!-- HTML -->
								<div id="accordion">
									<h4 class="accordion-toggle"><span>01</span>Account Details</h4>
									<div class="accordion-content default"> 
										<div class="details-box">
											<p>My Tusk ID: <?php echo str_pad($user_details[0]->id, 10, '0', STR_PAD_LEFT) ?></p>
											<p>Complete Name: <?php echo $user_details[0]->fullname; ?></p>
											<p>Address: <?php echo $user_details[0]->address; ?></p>
											<p>Zip Code: <?php echo $user_details[0]->zip; ?></p>
											<p>Country: <?php echo $user_details[0]->country; ?></p>
											<p>Membership Expiration: <?php echo date("F d, Y", strtotime($user_details[0]->date_expired)); ?></p>
											
										</div>
									</div>  
									<div class="clearfix"></div>                               
								</div>
                        </div>
						<div class="col-md-4 col-sm-5">
                            <!-- HTML -->
								<div id="accordion">
									<h4 class="accordion-toggle"><span>02</span>Contact Numbers</h4>
									<div class="accordion-content default"> 
										<div class="details-box">
											<p>Country Code: +<?php echo $user_details[0]->cc_code; ?><p/>
											<p>Contact Number: <?php echo $user_details[0]->contact1; ?><p/>
											<p>Email: <?php echo $user_details[0]->email; ?><p/>
											<?php if($user_details[0]->verified == 0){ ?>
												<button class="btn btn-default" id="verifymenow">Verify my email</button>
											<?php }else{ ?>
												<p class="verified">Email Verified</p>
											<?php } ?>
											<br/>
											<br/>
										</div>
									</div>  
									<div class="clearfix"></div>                               
								</div>
                        </div>
                        <div class="col-md-3 col-sm-3 checkout-steps">
                            <h6>My Account</h6>
                            <div>
								<?php if($this->session->userdata('role') == 2){ ?>
									<ul class="account-list">
										<li><a href="<?php echo base_url();?>admin" target="_blank"> <i class="fa fa-edit"></i> Manage Users</a></li>  
									</ul> 
									<br/>
								<?php }?>
								
                                <ul class="account-list">
									<li class="active"><a href="<?php echo base_url();?>account"> <i class="fa fa-edit"></i> My Account</a></li>   
                                    <li><a href="<?php echo base_url();?>account/edit"> <i class="fa fa-edit"></i> Edit Account</a></li>                     
                                    <li><a href="<?php echo base_url();?>account/changepassword"> <i class="fa fa-edit"></i> Change Password</a></li>
                                    <li><a href="<?php echo base_url();?>auction/items-list"> <i class="fa fa-edit"></i> Auction Items</a></li>
									<li><a href="<?php echo base_url();?>auction/bought"> <i class="fa fa-edit"></i> Bought Auction Items</a></li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>
			<div class="modal fade" id="verifyMessage" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>

							<div class="text-center">
								<h3 class="agileinfo_sign">Go to your inbox and check the verification email.</h3>
							</div>
						</div>
					</div>
				</div>		
			</div>