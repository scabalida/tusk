          <!-- POLICY -->
            <div class="policy-item" id="policy2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="pi-wrap">
                                <i class="fa fa-plane"></i>
                                <h4>shipping<span>Write shipping options in your auctions.</span></h4>
                                <p>Shipping, Collect or Delivery will be arranged between seller and buyer AFTER the auction is ended.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="pi-wrap">
                                <i class="fa fa-money"></i>
                                <h4>Payment<span>All payment will be between seller and buyer</span></h4>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="pi-wrap">
                                <i class="fa fa-clock-o"></i>
                                <h4>Store Hours<span>Open: 0-24-52-365</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FOOTER -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 widget-footer">
                            <img src="<?php echo base_url();?>images/logo-tusk.png" class="img-responsive space10" alt=""/>
                            <p>www.tusk.nu is the auction site for everybody. And with the lowest cost of all the auction sites that exist.</p>
                            <div class="clearfix"></div>
                            <ul class="f-social">
                                <li><a href="https://www.facebook.com/tusk.nu/" class="fa fa-facebook"></a></li>
                                <li><a href="mailto:tusk.nu@gmail.com" class="fa fa-envelope"></a></li>
                                <li><a href="https://www.instagram.com/tusk.nu/" class="fa fa-instagram"></a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-4 widget-footer">
                            <h5>Categories</h5>
                            <ul class="widget-tags">
                                <li><a href="<?php echo base_url(); ?>auction/categories/carpets-rugs-and-textiles">Carpets, rugs and textiles</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/clothing-shoes-and-accessories">Clothing</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/electronics">Electronics</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/furniture">Furniture</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/jewellery">Jewellery</a></li>
								<li><a href="<?php echo base_url(); ?>auction/categories/travel-and-experiences">Travel and experiences</a></li>
                                <li><a href="<?php echo base_url(); ?>auction/categories/watches-and-clocks">Watches and clocks</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-4 widget-footer">
                            <h5>Contact Us</h5>
                            <p>Email: tusk.nu@gmail.com</p>
                        </div>
                    </div>
                </div>
            </footer>

            <!-- FOOTER COPYRIGHT -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-sm-7">
                            <br>
                            <p>Copyright 2019 &middot; All rights reserved</p>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <img src="<?php echo base_url();?>images/template/basic/payment.png" class="pull-right img-responsive payment" alt=""/>
                        </div>
                    </div>
                </div>
            </div>	

        </div>

        <div id="backtotop"><i class="fa fa-chevron-up"></i></div>

        <!-- Javascript -->
        <script src="<?php echo base_url();?>js/template/jquery.js"></script>

        <!-- ADDTHIS -->
        <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-557a95e76b3e51d9" async="async"></script>
        <script type="text/javascript">
            function loadAddThis() {
                addthis.init()
            }
        </script>
        <script src="<?php echo base_url();?>js/template/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>plugin/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url();?>js/template/bs-navbar.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/isotope/isotope.pkgd.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/slick/slick.min.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/tweets/tweecool.min.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url();?>js/template/jquery.sticky.js"></script>
        <script src="<?php echo base_url();?>js/template/jquery.subscribe-better.js"></script>
        <script src="<?php echo base_url();?>js/template/jquery-ui.min.js"></script>
        <script src="<?php echo base_url();?>js/template/validator.min.js"></script>
        <script src="<?php echo base_url();?>js/template/vendors/select/jquery.selectBoxIt.min.js"></script>
        <script src="<?php echo base_url();?>js/template/main.js"></script>
        <script src="<?php echo base_url();?>js/template/flipclock.min.js"></script>
        <script src="<?php echo base_url();?>js/template/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>js/template/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo base_url();?>js/template/dataTables.responsive.min.js"></script>
		
        <?php if(current_url() == site_url('contact-us')){?>
			<script src="<?php echo base_url();?>js/contact-us.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url('account') || current_url() == site_url('account/register') || current_url() == site_url('account/edit') || current_url() == site_url('account/changepassword')){?>
			<script src="<?php echo base_url();?>js/account.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url('login')){?>
			<script src="<?php echo base_url();?>js/login.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url('admin')){?>
			<script src="<?php echo base_url();?>js/admin.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url() || current_url() == site_url('home')){?>
			<script src="<?php echo base_url();?>js/home.js"></script>
		<?php } ?>
		<?php if(current_url() == site_url('auction/bought') || current_url() == site_url('soon-ending') || current_url() == site_url('auction') || current_url() == site_url('auction/add') || current_url() == site_url('auction/items-list')){?>
			<script src="<?php echo base_url();?>js/auction.js"></script>
		<?php } ?>
		<?php 
			$currentURL = current_url();
			$chckURL = strpos($currentURL, "categories");
			//echo $chckURL;
			if($chckURL == 23){
		?>
			<script src="<?php echo base_url();?>js/auction.js"></script>
		<?php } ?>
    </body>
</html>