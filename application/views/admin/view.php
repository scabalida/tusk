
            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Manage Users</li>
                    </ul>
                </div>
            </div>


            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
                    <div class="row">
						<div class="alert alert-success display-none" id="updateUserSuccess">
							<p><strong>Success!</strong> User's data successfully updated.</p>
						</div>
						<div class="alert alert-danger display-none" id="updateUserError">
							<p><strong>Error!</strong> User's data unsuccessfully updated.</p>
						</div>
						<div class="alert alert-success display-none" id="deleteUserSuccess">
							<p><strong>Success!</strong> User's data successfully deleted.</p>
						</div>
						<div class="alert alert-danger display-none" id="deleteUserError">
							<p><strong>Error!</strong> User's data unsuccessfully deleted.</p>
						</div>
						
                        <div class="col-md-12 col-sm-12">
                            <div id="account-id">
								<h4 class="pull-right">Unique Visitors: <span class="label label-danger"><?php echo $visitors; ?></span></h4>
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>Manage Users</h4>                                                                 
                               
									<?php if(count($users) >0 ){?>
										<table id="allUsersTable" class="table table-striped table-bordered responsive" style="width:100%;">
											<thead>
												<tr>
													<th scope="col text-center">Tusk ID</th>
													<th scope="col text-center">Full Name</th>
													<th scope="col text-center">Email</th>
													<th scope="col text-center">Contact Number</th>
													<th scope="col text-center">Address</th>
													<th scope="col text-center">Zip</th>
													<th scope="col text-center">Country</th>
													<th scope="col text-center"></th>
												</tr>
											</thead>
											<tbody id="tbodyallUsers">
												<?php foreach($users as $row){?>	
													<div class="modal fade" id="deleteUserModal<?php echo $row->id; ?>" tabindex="-1" role="dialog">
														<div class="modal-dialog">
															<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>

																	<div class="text-center">
																		<h3 class="agileinfo_sign">REMOVE THIS USER?</h3>
																		<form onsubmit="return delete_user_form(<?php echo $row->id; ?>);" method="post">
																			<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
																			<button type="submit" class="btn btn-danger">DELETE</button>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal fade" id="updateUserModal<?php echo $row->id; ?>" tabindex="-1" role="dialog">
														<div class="modal-dialog">
															<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																		<h3>Update User Details</h3>
																		<div>
																			<form onsubmit="return update_user_form(<?php echo $row->id; ?>);" method="post" data-toggle="validator" role="form">
																				<ul class="form-list row">
																					<li class="col-md-6 col-sm-6">
																						<div class="form-group">
																							<label >Complete Name</label>
																							<input required type="text" name="fullname" id="fullname<?php echo $row->id; ?>" value="<?php echo $row->fullname; ?>" class="form-control input-text">
																							<div class="help-block with-errors"></div>
																						</div>
																					</li>                                             
																					<li class="col-md-6 col-sm-6">
																						<div class="form-group">
																							<label >Birthdate</label>
																							<input required type="date" name="bday" id="bday<?php echo $row->id; ?>" value="<?php echo $row->bday; ?>"  class="form-control input-text">
																							<input type="hidden" name="id" id="id" value="<?php echo $row->id; ?>"  class="form-control input-text">
																							<div class="help-block with-errors"></div>
																						</div>
																					</li>
																					<li class="col-md-12 col-sm-12">
																						<div class="form-group">
																							<label>Address</label>
																							<input type="text" class="form-control input-text" value="<?php echo $row->address; ?>"  name="address" id="address<?php echo $row->id; ?>" required>
																							<div class="help-block with-errors"></div>
																						</div>
																					</li>
																					<li class="col-md-6 col-sm-6">
																						<div class="form-group">
																							<label>Zip Code</label>
																							<input type="text" class="form-control input-text" value="<?php echo $row->zip; ?>"  name="zip" id="zip<?php echo $row->id; ?>" required>
																							<div class="help-block with-errors"></div>
																						</div>
																					</li>
																					<li class="col-md-6 col-sm-6">
																						<div class="form-group">
																							<label>Country</label>
																							<input type="text" class="form-control input-text" value="<?php echo $row->country; ?>"  name="country" id="country<?php echo $row->id; ?>" required>
																							<div class="help-block with-errors"></div>
																						</div>
																					</li>
																					<li class="col-md-6 col-sm-6">
																						<div class="form-group">
																							<label>Country Code</label>
																							<input type="hidden" value="<?php echo $row->cc_code; ?>"  name="cc_c" id="cc_c<?php echo $row->id; ?>">
																							<select name="cc_code" class="form-control" id="cc_code<?php echo $row->id; ?>" required>
																								<option data-countryCode="DZ" <?php if($row->cc_code == 213){echo 'selected'; } ?> value="213">Algeria (+213)</option>
																								<option data-countryCode="AD" <?php if($row->cc_code == 376){echo 'selected'; } ?> value="376">Andorra (+376)</option>
																								<option data-countryCode="AO" <?php if($row->cc_code == 244){echo 'selected'; } ?> value="244">Angola (+244)</option>
																								<option data-countryCode="AI" <?php if($row->cc_code == 1264){echo 'selected'; } ?> value="1264">Anguilla (+1264)</option>
																								<option data-countryCode="AG" <?php if($row->cc_code == 1268){echo 'selected'; } ?> value="1268">Antigua &amp; Barbuda (+1268)</option>
																								<option data-countryCode="AR" <?php if($row->cc_code == 54){echo 'selected'; } ?> value="54">Argentina (+54)</option>
																								<option <?php if($row->cc_code == 374){echo 'selected'; } ?> data-countryCode="AM" value="374">Armenia (+374)</option>
																								<option <?php if($row->cc_code == 297){echo 'selected'; } ?> data-countryCode="AW" value="297">Aruba (+297)</option>
																								<option <?php if($row->cc_code == 61){echo 'selected'; } ?> data-countryCode="AU" value="61">Australia (+61)</option>
																								<option <?php if($row->cc_code == 43){echo 'selected'; } ?> data-countryCode="AT" value="43">Austria (+43)</option>
																								<option <?php if($row->cc_code == 994){echo 'selected'; } ?> data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
																								<option <?php if($row->cc_code == 1242){echo 'selected'; } ?> data-countryCode="BS" value="1242">Bahamas (+1242)</option>
																								<option <?php if($row->cc_code == 973){echo 'selected'; } ?> data-countryCode="BH" value="973">Bahrain (+973)</option>
																								<option <?php if($row->cc_code == 880){echo 'selected'; } ?> data-countryCode="BD" value="880">Bangladesh (+880)</option>
																								<option <?php if($row->cc_code == 1246){echo 'selected'; } ?> data-countryCode="BB" value="1246">Barbados (+1246)</option>
																								<option <?php if($row->cc_code == 375){echo 'selected'; } ?> data-countryCode="BY" value="375">Belarus (+375)</option>
																								<option <?php if($row->cc_code == 32){echo 'selected'; } ?> data-countryCode="BE" value="32">Belgium (+32)</option>
																								<option <?php if($row->cc_code == 501){echo 'selected'; } ?> data-countryCode="BZ" value="501">Belize (+501)</option>
																								<option <?php if($row->cc_code == 229){echo 'selected'; } ?> data-countryCode="BJ" value="229">Benin (+229)</option>
																								<option <?php if($row->cc_code == 1441){echo 'selected'; } ?> data-countryCode="BM" value="1441">Bermuda (+1441)</option>
																								<option <?php if($row->cc_code == 975){echo 'selected'; } ?> data-countryCode="BT" value="975">Bhutan (+975)</option>
																								<option <?php if($row->cc_code == 591){echo 'selected'; } ?> data-countryCode="BO" value="591">Bolivia (+591)</option>
																								<option <?php if($row->cc_code == 387){echo 'selected'; } ?> data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
																								<option <?php if($row->cc_code == 267){echo 'selected'; } ?> data-countryCode="BW" value="267">Botswana (+267)</option>
																								<option <?php if($row->cc_code == 55){echo 'selected'; } ?> data-countryCode="BR" value="55">Brazil (+55)</option>
																								<option <?php if($row->cc_code == 673){echo 'selected'; } ?> data-countryCode="BN" value="673">Brunei (+673)</option>
																								<option <?php if($row->cc_code == 359){echo 'selected'; } ?> data-countryCode="BG" value="359">Bulgaria (+359)</option>
																								<option <?php if($row->cc_code == 226){echo 'selected'; } ?> data-countryCode="BF" value="226">Burkina Faso (+226)</option>
																								<option <?php if($row->cc_code == 257){echo 'selected'; } ?> data-countryCode="BI" value="257">Burundi (+257)</option>
																								<option <?php if($row->cc_code == 855){echo 'selected'; } ?> data-countryCode="KH" value="855">Cambodia (+855)</option>
																								<option <?php if($row->cc_code == 237){echo 'selected'; } ?> data-countryCode="CM" value="237">Cameroon (+237)</option>
																								<option <?php if($row->cc_code == 1){echo 'selected'; } ?> data-countryCode="CA" value="1">Canada (+1)</option>
																								<option <?php if($row->cc_code == 238){echo 'selected'; } ?> data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
																								<option <?php if($row->cc_code == 1345){echo 'selected'; } ?> data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
																								<option <?php if($row->cc_code == 236){echo 'selected'; } ?> data-countryCode="CF" value="236">Central African Republic (+236)</option>
																								<option <?php if($row->cc_code == 56){echo 'selected'; } ?> data-countryCode="CL" value="56">Chile (+56)</option>
																								<option <?php if($row->cc_code == 86){echo 'selected'; } ?> data-countryCode="CN" value="86">China (+86)</option>
																								<option <?php if($row->cc_code == 57){echo 'selected'; } ?> data-countryCode="CO" value="57">Colombia (+57)</option>
																								<option <?php if($row->cc_code == 269){echo 'selected'; } ?> data-countryCode="KM" value="269">Comoros (+269)</option>
																								<option <?php if($row->cc_code == 242){echo 'selected'; } ?> data-countryCode="CG" value="242">Congo (+242)</option>
																								<option <?php if($row->cc_code == 682){echo 'selected'; } ?> data-countryCode="CK" value="682">Cook Islands (+682)</option>
																								<option <?php if($row->cc_code == 506){echo 'selected'; } ?> data-countryCode="CR" value="506">Costa Rica (+506)</option>
																								<option <?php if($row->cc_code == 385){echo 'selected'; } ?> data-countryCode="HR" value="385">Croatia (+385)</option>
																								<option <?php if($row->cc_code == 53){echo 'selected'; } ?> data-countryCode="CU" value="53">Cuba (+53)</option>
																								<option <?php if($row->cc_code == 90392){echo 'selected'; } ?> data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
																								<option <?php if($row->cc_code == 357){echo 'selected'; } ?> data-countryCode="CY" value="357">Cyprus South (+357)</option>
																								<option <?php if($row->cc_code == 42){echo 'selected'; } ?> data-countryCode="CZ" value="42">Czech Republic (+42)</option>
																								<option <?php if($row->cc_code == 45){echo 'selected'; } ?> data-countryCode="DK" value="45">Denmark (+45)</option>
																								<option <?php if($row->cc_code == 253){echo 'selected'; } ?> data-countryCode="DJ" value="253">Djibouti (+253)</option>
																								<option <?php if($row->cc_code == 1809){echo 'selected'; } ?> data-countryCode="DM" value="1809">Dominica (+1809)</option>
																								<option <?php if($row->cc_code == 1809){echo 'selected'; } ?> data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
																								<option <?php if($row->cc_code == 593){echo 'selected'; } ?> data-countryCode="EC" value="593">Ecuador (+593)</option>
																								<option <?php if($row->cc_code == 20){echo 'selected'; } ?> data-countryCode="EG" value="20">Egypt (+20)</option>
																								<option <?php if($row->cc_code == 503){echo 'selected'; } ?> data-countryCode="SV" value="503">El Salvador (+503)</option>
																								<option <?php if($row->cc_code == 240){echo 'selected'; } ?> data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
																								<option <?php if($row->cc_code == 291){echo 'selected'; } ?> data-countryCode="ER" value="291">Eritrea (+291)</option>
																								<option <?php if($row->cc_code == 372){echo 'selected'; } ?> data-countryCode="EE" value="372">Estonia (+372)</option>
																								<option <?php if($row->cc_code == 251){echo 'selected'; } ?> data-countryCode="ET" value="251">Ethiopia (+251)</option>
																								<option <?php if($row->cc_code == 500){echo 'selected'; } ?> data-countryCode="FK" value="500">Falkland Islands (+500)</option>
																								<option <?php if($row->cc_code == 298){echo 'selected'; } ?> data-countryCode="FO" value="298">Faroe Islands (+298)</option>
																								<option <?php if($row->cc_code == 679){echo 'selected'; } ?> data-countryCode="FJ" value="679">Fiji (+679)</option>
																								<option <?php if($row->cc_code == 358){echo 'selected'; } ?> data-countryCode="FI" value="358">Finland (+358)</option>
																								<option <?php if($row->cc_code == 33){echo 'selected'; } ?> data-countryCode="FR" value="33">France (+33)</option>
																								<option <?php if($row->cc_code == 594){echo 'selected'; } ?> data-countryCode="GF" value="594">French Guiana (+594)</option>
																								<option <?php if($row->cc_code == 689){echo 'selected'; } ?> data-countryCode="PF" value="689">French Polynesia (+689)</option>
																								<option <?php if($row->cc_code == 241){echo 'selected'; } ?> data-countryCode="GA" value="241">Gabon (+241)</option>
																								<option <?php if($row->cc_code == 220){echo 'selected'; } ?> data-countryCode="GM" value="220">Gambia (+220)</option>
																								<option <?php if($row->cc_code == 7880){echo 'selected'; } ?> data-countryCode="GE" value="7880">Georgia (+7880)</option>
																								<option <?php if($row->cc_code == 49){echo 'selected'; } ?> data-countryCode="DE" value="49">Germany (+49)</option>
																								<option <?php if($row->cc_code == 233){echo 'selected'; } ?> data-countryCode="GH" value="233">Ghana (+233)</option>
																								<option <?php if($row->cc_code == 350){echo 'selected'; } ?> data-countryCode="GI" value="350">Gibraltar (+350)</option>
																								<option <?php if($row->cc_code == 30){echo 'selected'; } ?> data-countryCode="GR" value="30">Greece (+30)</option>
																								<option <?php if($row->cc_code == 299){echo 'selected'; } ?> data-countryCode="GL" value="299">Greenland (+299)</option>
																								<option <?php if($row->cc_code == 1473){echo 'selected'; } ?> data-countryCode="GD" value="1473">Grenada (+1473)</option>
																								<option <?php if($row->cc_code == 590){echo 'selected'; } ?> data-countryCode="GP" value="590">Guadeloupe (+590)</option>
																								<option <?php if($row->cc_code == 671){echo 'selected'; } ?> data-countryCode="GU" value="671">Guam (+671)</option>
																								<option <?php if($row->cc_code == 502){echo 'selected'; } ?> data-countryCode="GT" value="502">Guatemala (+502)</option>
																								<option <?php if($row->cc_code == 224){echo 'selected'; } ?> data-countryCode="GN" value="224">Guinea (+224)</option>
																								<option <?php if($row->cc_code == 245){echo 'selected'; } ?> data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
																								<option <?php if($row->cc_code == 592){echo 'selected'; } ?> data-countryCode="GY" value="592">Guyana (+592)</option>
																								<option <?php if($row->cc_code == 509){echo 'selected'; } ?> data-countryCode="HT" value="509">Haiti (+509)</option>
																								<option <?php if($row->cc_code == 504){echo 'selected'; } ?> data-countryCode="HN" value="504">Honduras (+504)</option>
																								<option <?php if($row->cc_code == 852){echo 'selected'; } ?> data-countryCode="HK" value="852">Hong Kong (+852)</option>
																								<option <?php if($row->cc_code == 36){echo 'selected'; } ?> data-countryCode="HU" value="36">Hungary (+36)</option>
																								<option <?php if($row->cc_code == 354){echo 'selected'; } ?> data-countryCode="IS" value="354">Iceland (+354)</option>
																								<option <?php if($row->cc_code == 91){echo 'selected'; } ?> data-countryCode="IN" value="91">India (+91)</option>
																								<option <?php if($row->cc_code == 62){echo 'selected'; } ?> data-countryCode="ID" value="62">Indonesia (+62)</option>
																								<option <?php if($row->cc_code == 98){echo 'selected'; } ?> data-countryCode="IR" value="98">Iran (+98)</option>
																								<option <?php if($row->cc_code == 964){echo 'selected'; } ?> data-countryCode="IQ" value="964">Iraq (+964)</option>
																								<option <?php if($row->cc_code == 353){echo 'selected'; } ?> data-countryCode="IE" value="353">Ireland (+353)</option>
																								<option <?php if($row->cc_code == 972){echo 'selected'; } ?> data-countryCode="IL" value="972">Israel (+972)</option>
																								<option <?php if($row->cc_code == 39){echo 'selected'; } ?> data-countryCode="IT" value="39">Italy (+39)</option>
																								<option <?php if($row->cc_code == 1876){echo 'selected'; } ?> data-countryCode="JM" value="1876">Jamaica (+1876)</option>
																								<option <?php if($row->cc_code == 81){echo 'selected'; } ?> data-countryCode="JP" value="81">Japan (+81)</option>
																								<option <?php if($row->cc_code == 962){echo 'selected'; } ?> data-countryCode="JO" value="962">Jordan (+962)</option>
																								<option <?php if($row->cc_code == 7){echo 'selected'; } ?> data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
																								<option <?php if($row->cc_code == 254){echo 'selected'; } ?> data-countryCode="KE" value="254">Kenya (+254)</option>
																								<option <?php if($row->cc_code == 686){echo 'selected'; } ?> data-countryCode="KI" value="686">Kiribati (+686)</option>
																								<option <?php if($row->cc_code == 850){echo 'selected'; } ?> data-countryCode="KP" value="850">Korea North (+850)</option>
																								<option <?php if($row->cc_code == 82){echo 'selected'; } ?> data-countryCode="KR" value="82">Korea South (+82)</option>
																								<option <?php if($row->cc_code == 965){echo 'selected'; } ?> data-countryCode="KW" value="965">Kuwait (+965)</option>
																								<option <?php if($row->cc_code == 996){echo 'selected'; } ?> data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
																								<option <?php if($row->cc_code == 856){echo 'selected'; } ?> data-countryCode="LA" value="856">Laos (+856)</option>
																								<option <?php if($row->cc_code == 371){echo 'selected'; } ?> data-countryCode="LV" value="371">Latvia (+371)</option>
																								<option <?php if($row->cc_code == 961){echo 'selected'; } ?> data-countryCode="LB" value="961">Lebanon (+961)</option>
																								<option <?php if($row->cc_code == 266){echo 'selected'; } ?> data-countryCode="LS" value="266">Lesotho (+266)</option>
																								<option <?php if($row->cc_code == 231){echo 'selected'; } ?> data-countryCode="LR" value="231">Liberia (+231)</option>
																								<option <?php if($row->cc_code == 218){echo 'selected'; } ?> data-countryCode="LY" value="218">Libya (+218)</option>
																								<option <?php if($row->cc_code == 417){echo 'selected'; } ?> data-countryCode="LI" value="417">Liechtenstein (+417)</option>
																								<option <?php if($row->cc_code == 370){echo 'selected'; } ?> data-countryCode="LT" value="370">Lithuania (+370)</option>
																								<option <?php if($row->cc_code == 352){echo 'selected'; } ?> data-countryCode="LU" value="352">Luxembourg (+352)</option>
																								<option <?php if($row->cc_code == 853){echo 'selected'; } ?> data-countryCode="MO" value="853">Macao (+853)</option>
																								<option <?php if($row->cc_code == 389){echo 'selected'; } ?> data-countryCode="MK" value="389">Macedonia (+389)</option>
																								<option <?php if($row->cc_code == 261){echo 'selected'; } ?> data-countryCode="MG" value="261">Madagascar (+261)</option>
																								<option <?php if($row->cc_code == 265){echo 'selected'; } ?> data-countryCode="MW" value="265">Malawi (+265)</option>
																								<option <?php if($row->cc_code == 60){echo 'selected'; } ?> data-countryCode="MY" value="60">Malaysia (+60)</option>
																								<option <?php if($row->cc_code == 960){echo 'selected'; } ?> data-countryCode="MV" value="960">Maldives (+960)</option>
																								<option <?php if($row->cc_code == 223){echo 'selected'; } ?> data-countryCode="ML" value="223">Mali (+223)</option>
																								<option <?php if($row->cc_code == 356){echo 'selected'; } ?> data-countryCode="MT" value="356">Malta (+356)</option>
																								<option <?php if($row->cc_code == 692){echo 'selected'; } ?> data-countryCode="MH" value="692">Marshall Islands (+692)</option>
																								<option <?php if($row->cc_code == 596){echo 'selected'; } ?> data-countryCode="MQ" value="596">Martinique (+596)</option>
																								<option <?php if($row->cc_code == 222){echo 'selected'; } ?> data-countryCode="MR" value="222">Mauritania (+222)</option>
																								<option <?php if($row->cc_code == 269){echo 'selected'; } ?> data-countryCode="YT" value="269">Mayotte (+269)</option>
																								<option <?php if($row->cc_code == 52){echo 'selected'; } ?> data-countryCode="MX" value="52">Mexico (+52)</option>
																								<option <?php if($row->cc_code == 691){echo 'selected'; } ?> data-countryCode="FM" value="691">Micronesia (+691)</option>
																								<option <?php if($row->cc_code == 373){echo 'selected'; } ?> data-countryCode="MD" value="373">Moldova (+373)</option>
																								<option <?php if($row->cc_code == 377){echo 'selected'; } ?> data-countryCode="MC" value="377">Monaco (+377)</option>
																								<option <?php if($row->cc_code == 976){echo 'selected'; } ?> data-countryCode="MN" value="976">Mongolia (+976)</option>
																								<option <?php if($row->cc_code == 1664){echo 'selected'; } ?> data-countryCode="MS" value="1664">Montserrat (+1664)</option>
																								<option <?php if($row->cc_code == 212){echo 'selected'; } ?> data-countryCode="MA" value="212">Morocco (+212)</option>
																								<option <?php if($row->cc_code == 258){echo 'selected'; } ?> data-countryCode="MZ" value="258">Mozambique (+258)</option>
																								<option <?php if($row->cc_code == 95){echo 'selected'; } ?> data-countryCode="MN" value="95">Myanmar (+95)</option>
																								<option <?php if($row->cc_code == 264){echo 'selected'; } ?> data-countryCode="NA" value="264">Namibia (+264)</option>
																								<option <?php if($row->cc_code == 674){echo 'selected'; } ?> data-countryCode="NR" value="674">Nauru (+674)</option>
																								<option <?php if($row->cc_code == 977){echo 'selected'; } ?> data-countryCode="NP" value="977">Nepal (+977)</option>
																								<option <?php if($row->cc_code == 31){echo 'selected'; } ?> data-countryCode="NL" value="31">Netherlands (+31)</option>
																								<option <?php if($row->cc_code == 687){echo 'selected'; } ?> data-countryCode="NC" value="687">New Caledonia (+687)</option>
																								<option <?php if($row->cc_code == 64){echo 'selected'; } ?> data-countryCode="NZ" value="64">New Zealand (+64)</option>
																								<option <?php if($row->cc_code == 505){echo 'selected'; } ?> data-countryCode="NI" value="505">Nicaragua (+505)</option>
																								<option <?php if($row->cc_code == 227){echo 'selected'; } ?> data-countryCode="NE" value="227">Niger (+227)</option>
																								<option <?php if($row->cc_code == 234){echo 'selected'; } ?> data-countryCode="NG" value="234">Nigeria (+234)</option>
																								<option <?php if($row->cc_code == 683){echo 'selected'; } ?> data-countryCode="NU" value="683">Niue (+683)</option>
																								<option <?php if($row->cc_code == 672){echo 'selected'; } ?> data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
																								<option <?php if($row->cc_code == 670){echo 'selected'; } ?> data-countryCode="NP" value="670">Northern Marianas (+670)</option>
																								<option <?php if($row->cc_code == 47){echo 'selected'; } ?> data-countryCode="NO" value="47">Norway (+47)</option>
																								<option <?php if($row->cc_code == 968){echo 'selected'; } ?> data-countryCode="OM" value="968">Oman (+968)</option>
																								<option <?php if($row->cc_code == 680){echo 'selected'; } ?> data-countryCode="PW" value="680">Palau (+680)</option>
																								<option <?php if($row->cc_code == 507){echo 'selected'; } ?> data-countryCode="PA" value="507">Panama (+507)</option>
																								<option <?php if($row->cc_code == 675){echo 'selected'; } ?> data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
																								<option <?php if($row->cc_code == 595){echo 'selected'; } ?> data-countryCode="PY" value="595">Paraguay (+595)</option>
																								<option <?php if($row->cc_code == 51){echo 'selected'; } ?> data-countryCode="PE" value="51">Peru (+51)</option>
																								<option <?php if($row->cc_code == 63){echo 'selected'; } ?> data-countryCode="PH" value="63">Philippines (+63)</option>
																								<option <?php if($row->cc_code == 48){echo 'selected'; } ?> data-countryCode="PL" value="48">Poland (+48)</option>
																								<option <?php if($row->cc_code == 351){echo 'selected'; } ?> data-countryCode="PT" value="351">Portugal (+351)</option>
																								<option <?php if($row->cc_code == 1787){echo 'selected'; } ?> data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
																								<option <?php if($row->cc_code == 974){echo 'selected'; } ?> data-countryCode="QA" value="974">Qatar (+974)</option>
																								<option <?php if($row->cc_code == 262){echo 'selected'; } ?> data-countryCode="RE" value="262">Reunion (+262)</option>
																								<option <?php if($row->cc_code == 40){echo 'selected'; } ?> data-countryCode="RO" value="40">Romania (+40)</option>
																								<option <?php if($row->cc_code == 7){echo 'selected'; } ?> data-countryCode="RU" value="7">Russia (+7)</option>
																								<option <?php if($row->cc_code == 250){echo 'selected'; } ?> data-countryCode="RW" value="250">Rwanda (+250)</option>
																								<option <?php if($row->cc_code == 378){echo 'selected'; } ?> data-countryCode="SM" value="378">San Marino (+378)</option>
																								<option <?php if($row->cc_code == 239){echo 'selected'; } ?> data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
																								<option <?php if($row->cc_code == 966){echo 'selected'; } ?> data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
																								<option <?php if($row->cc_code == 221){echo 'selected'; } ?> data-countryCode="SN" value="221">Senegal (+221)</option>
																								<option <?php if($row->cc_code == 381){echo 'selected'; } ?> data-countryCode="CS" value="381">Serbia (+381)</option>
																								<option <?php if($row->cc_code == 248){echo 'selected'; } ?> data-countryCode="SC" value="248">Seychelles (+248)</option>
																								<option <?php if($row->cc_code == 232){echo 'selected'; } ?> data-countryCode="SL" value="232">Sierra Leone (+232)</option>
																								<option <?php if($row->cc_code == 65){echo 'selected'; } ?> data-countryCode="SG" value="65">Singapore (+65)</option>
																								<option <?php if($row->cc_code == 421){echo 'selected'; } ?> data-countryCode="SK" value="421">Slovak Republic (+421)</option>
																								<option <?php if($row->cc_code == 386){echo 'selected'; } ?> data-countryCode="SI" value="386">Slovenia (+386)</option>
																								<option <?php if($row->cc_code == 677){echo 'selected'; } ?> data-countryCode="SB" value="677">Solomon Islands (+677)</option>
																								<option <?php if($row->cc_code == 252){echo 'selected'; } ?> data-countryCode="SO" value="252">Somalia (+252)</option>
																								<option <?php if($row->cc_code == 27){echo 'selected'; } ?> data-countryCode="ZA" value="27">South Africa (+27)</option>
																								<option <?php if($row->cc_code == 34){echo 'selected'; } ?> data-countryCode="ES" value="34">Spain (+34)</option>
																								<option <?php if($row->cc_code == 94){echo 'selected'; } ?> data-countryCode="LK" value="94">Sri Lanka (+94)</option>
																								<option <?php if($row->cc_code == 290){echo 'selected'; } ?> data-countryCode="SH" value="290">St. Helena (+290)</option>
																								<option <?php if($row->cc_code == 1869){echo 'selected'; } ?> data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
																								<option <?php if($row->cc_code == 1758){echo 'selected'; } ?> data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
																								<option <?php if($row->cc_code == 249){echo 'selected'; } ?> data-countryCode="SD" value="249">Sudan (+249)</option>
																								<option <?php if($row->cc_code == 597){echo 'selected'; } ?> data-countryCode="SR" value="597">Suriname (+597)</option>
																								<option <?php if($row->cc_code == 268){echo 'selected'; } ?> data-countryCode="SZ" value="268">Swaziland (+268)</option>
																								<option <?php if($row->cc_code == 46){echo 'selected'; } ?> data-countryCode="SE" value="46">Sweden (+46)</option>
																								<option <?php if($row->cc_code == 41){echo 'selected'; } ?> data-countryCode="CH" value="41">Switzerland (+41)</option>
																								<option <?php if($row->cc_code == 963){echo 'selected'; } ?> data-countryCode="SI" value="963">Syria (+963)</option>
																								<option <?php if($row->cc_code == 886){echo 'selected'; } ?> data-countryCode="TW" value="886">Taiwan (+886)</option>
																								<option <?php if($row->cc_code == 7){echo 'selected'; } ?> data-countryCode="TJ" value="7">Tajikstan (+7)</option>
																								<option <?php if($row->cc_code == 66){echo 'selected'; } ?> data-countryCode="TH" value="66">Thailand (+66)</option>
																								<option <?php if($row->cc_code == 228){echo 'selected'; } ?> data-countryCode="TG" value="228">Togo (+228)</option>
																								<option <?php if($row->cc_code == 676){echo 'selected'; } ?> data-countryCode="TO" value="676">Tonga (+676)</option>
																								<option <?php if($row->cc_code == 1868){echo 'selected'; } ?> data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
																								<option <?php if($row->cc_code == 216){echo 'selected'; } ?> data-countryCode="TN" value="216">Tunisia (+216)</option>
																								<option <?php if($row->cc_code == 90){echo 'selected'; } ?> data-countryCode="TR" value="90">Turkey (+90)</option>
																								<option <?php if($row->cc_code == 7){echo 'selected'; } ?> data-countryCode="TM" value="7">Turkmenistan (+7)</option>
																								<option <?php if($row->cc_code == 993){echo 'selected'; } ?> data-countryCode="TM" value="993">Turkmenistan (+993)</option>
																								<option <?php if($row->cc_code == 1649){echo 'selected'; } ?> data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
																								<option <?php if($row->cc_code == 688){echo 'selected'; } ?> data-countryCode="TV" value="688">Tuvalu (+688)</option>
																								<option <?php if($row->cc_code == 256){echo 'selected'; } ?> data-countryCode="UG" value="256">Uganda (+256)</option>
																								<option <?php if($row->cc_code == 44){echo 'selected'; } ?> data-countryCode="GB" value="44">UK (+44)</option>
																								<option <?php if($row->cc_code == 380){echo 'selected'; } ?> data-countryCode="UA" value="380">Ukraine (+380)</option>
																								<option <?php if($row->cc_code == 971){echo 'selected'; } ?> data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
																								<option <?php if($row->cc_code == 598){echo 'selected'; } ?> data-countryCode="UY" value="598">Uruguay (+598)</option>
																								<option <?php if($row->cc_code == 1){echo 'selected'; } ?> data-countryCode="US" value="1">USA (+1)</option>
																								<option <?php if($row->cc_code == 7){echo 'selected'; } ?> data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
																								<option <?php if($row->cc_code == 678){echo 'selected'; } ?> data-countryCode="VU" value="678">Vanuatu (+678)</option>
																								<option <?php if($row->cc_code == 379){echo 'selected'; } ?> data-countryCode="VA" value="379">Vatican City (+379)</option>
																								<option <?php if($row->cc_code == 58){echo 'selected'; } ?> data-countryCode="VE" value="58">Venezuela (+58)</option>
																								<option <?php if($row->cc_code == 84){echo 'selected'; } ?> data-countryCode="VN" value="84">Vietnam (+84)</option>
																								<option <?php if($row->cc_code == 84){echo 'selected'; } ?> data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
																								<option <?php if($row->cc_code == 84){echo 'selected'; } ?> data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
																								<option <?php if($row->cc_code == 681){echo 'selected'; } ?> data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
																								<option <?php if($row->cc_code == 969){echo 'selected'; } ?> data-countryCode="YE" value="969">Yemen (North)(+969)</option>
																								<option <?php if($row->cc_code == 967){echo 'selected'; } ?> data-countryCode="YE" value="967">Yemen (South)(+967)</option>
																								<option <?php if($row->cc_code == 260){echo 'selected'; } ?> data-countryCode="ZM" value="260">Zambia (+260)</option>
																								<option <?php if($row->cc_code == 263){echo 'selected'; } ?> data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
																							</select>
																							<div class="help-block with-errors"></div>
																						</div>
																					</li>
																					
																					<li class="col-md-6 col-sm-6">
																						<div class="form-group">
																							<label>Contact Number</label>
																							<input type="text" name="contact1" id="contact1<?php echo $row->id; ?>" value="<?php echo $row->contact1; ?>"  class="form-control input-text" required>
																							<div class="help-block with-errors"></div>
																						</div>
																					</li>
																				</ul>
																				<div class="buttons-set text-right">
																					<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
																					<button class="btn-black" type="submit"><span><span>SAVE</span></span></button>
																				</div>
																			</form>
																		</div>
																</div>
															</div>
														</div>
													</div>
													<tr>
														<td><?php echo $row->id; ?></td>
														<td><?php echo ucfirst($row->fullname); ?></td>
														<td><?php echo $row->email; ?></td>
														<td><?php echo "+" . $row->cc_code . " " . $row->contact1; ?></td>
														<td><?php echo $row->address; ?></td>
														<td><?php echo $row->zip; ?></td>
														<td><?php echo $row->country; ?></td>
														<td>
															<div class="dropdown">
															  <a class="btn btn-primary dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-cog"></i>
															  <span class="caret"></span></a>
															  <ul class="dropdown-menu" style="right:0;left: unset;">
																<li><a href="#" data-toggle="modal" data-target="#updateUserModal<?php echo $row->id; ?>" style=" color: #3c763d;"><i class="fa fa-pencil"></i> Edit</a></li>
																<li><a href="#" data-toggle="modal" data-target="#deleteUserModal<?php echo $row->id; ?>" style=" color: #a94442;"><i class="fa fa-trash"></i> Remove</a></li>
															  </ul>
															</div>
														</td>
													</tr>
												<?php } ?>								
											</tbody>
										</table>
									<?php }else{?>
									<div class="alert alert-info" role="alert">
										<h4>No data to be displayed.</h4>
									</div>
									<?php }?>
                                                                
                            </div>
                        
						</div>
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>
