<!DOCTYPE html>
	<html>
    <head>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Tusk</title>

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="icon" type="image/jpg" href="<?php echo base_url();?>images/favicon.jpg">

        <!-- Google Webfont -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300,300italic,700,900' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>css/template/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url();?>css/template/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>css/template/flipclock.css">
        <link rel="stylesheet" href="<?php echo base_url();?>js/template/vendors/isotope/isotope.css">
        <link rel="stylesheet" href="<?php echo base_url();?>js/template/vendors/slick/slick.css">
        <link rel="stylesheet" href="<?php echo base_url();?>js/template/vendors/rs-plugin/css/settings.css">
        <link rel="stylesheet" href="<?php echo base_url();?>js/template/vendors/select/jquery.selectBoxIt.css">
        <link rel="stylesheet" href="<?php echo base_url();?>css/template/subscribe-better.css">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url();?>plugin/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url();?>plugin/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="<?php echo base_url();?>css/template/style.css">
        <link rel="stylesheet" href="<?php echo base_url();?>css/template/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>css/template/responsive.dataTables.min.css">
		<script src="//connect.facebook.net/en_US/all.js"></script>
    </head>
    <body id="home2" class="header2 bg-tusk">
	<div id="fb-root"></div>
	
    <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({appId: '658236991325615', status: true, cookie: true, xfbml: true});
			
            };
	</script>
	
        <!-- PRELOADER -->
        <div id="loader"></div>

        <div class="body boxed">

            <!-- TOPBAR -->
            <div class="top_bar">
                <div class="container">
                    <div class="row">
                        <div class="row">
							<div class="col-md-12 col-sm-12">
                                <div class="tb_center pull-left">
                                    <ul>
                                        <li><i class="fa fa-envelope-o"></i> <a href="#">tusk.nu@gmail.com</a></li>
                                    </ul>
                                </div>
                                <div class="tb_right pull-right">
                                    <ul>
                                        <li>
                                            <div class="tbr-info">
												<?php if($this->session->userdata('loggedin') != TRUE){ ?>
													<span><a href="<?php echo base_url();?>login">Login</a></span>
												<?php }else{?>
													<span></span>
													<span>My Account<i class="fa fa-caret-down"></i></span>
													<div class="tbr-inner">
														<a href="<?php echo base_url();?>account">Account Details</a>
														<a href="<?php echo base_url();?>account/logout"> Log Out</a>
													</div>
												<?php }?>

                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- HEADER -->
            <header id="header2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/logo-tusk.png" class="img-responsive" alt=""/></a>
                        </div>
                        <div class="col-md-4 col-sm-4">
							
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <!-- Logo -->
                                </div>
                                <!-- Navmenu -->
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav text-center">
                                        <li class="dropdown"><a href="<?php echo base_url();?>" class="active">Home</a></li>
                                        <li class="dropdown"><a href="<?php echo base_url();?>about-us" class="">About Us</a></li>
                                        <li class="dropdown"><a href="<?php echo base_url();?>auction" class="">Auctions</a></li>
                                        <li class="dropdown"><a href="<?php echo base_url();?>soon-ending" class="">Soon Ending</a></li>
                                        <li class="dropdown"><a href="<?php echo base_url();?>contact-us" class="">Contact Us</a></li>

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>

            <div class="clearfix space10"></div>
