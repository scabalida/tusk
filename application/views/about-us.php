            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>About Us</li>
                    </ul>
                </div>
            </div>

            <div class="space10"></div>

            <!-- BLOG CONTENT -->
            <div class="blog-content">
                <div class="container">
                    <div class="row">
						<div class="col-md-12 col-sm-12 blog-content">
                            <div class="blog-single">
                                <article class="blogpost">
                                    <h2 class="post-title"><a href="#">About Tusk</a></h2>
  
                                    <!-- Media Gallery 
                                    <div class="post-media">
                                        <div class="blog-slider">
                                            <div class="item">						
                                                <img src="<?php echo base_url();?>images/template/blog/1.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="item">						
                                                <img src="<?php echo base_url();?>images/template/blog/2.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="item">						
                                                <img src="<?php echo base_url();?>images/template/blog/3.jpg"  class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="space30"></div>
                                    <p>
                                        www.tusk.nu is the auction site for everybody. And with the lowest cost of all the auction sites that exist. We don't have any minimum worth of the item as most of the auction sites have. And we don't have the hammer fee to pay for.
                                    </p>
                                    <p>
                                        Our goal is to have all the good deals out in the public, for everyone to bid on. Come and try us out. You won't be disappointed. A deal is a deal, no matter the cost is 100; or 1000;
                                    </p>
                                    <p>
                                        Create your profile, and start adding your items for auction in 1-2 minutes. If you want your item to be extra disposed, you can buy the option to have it on the front page under featured item. Create your auction in your preferred language, or in English. 
                                    </p>
                                    <p>
                                        All browsers can do any needed translation. Set your minimum prize, add at least 2 photos, and please be careful to describe how the item can be delivered. After the item is sold, the seller will be charged a small amount to www.tusk.nu. 
                                    </p>
                                    </p>
										After the payment is done, the seller will receive an email from buyer with all the needed contact info for paying and delivery. 
										Do you have any questions, please be free to contact us at tusk.nu@gmail.com
										And now : Go make some deals
									</p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix space20"></div>
