<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class home extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_auction');
		$this->load->model('m_admin');
    }
		public function index() {
			$data['featured'] = $this->m_auction->getAllFeaturedAuction();
			$data['recent'] = $this->m_auction->getAllRecentAuction();
			$data['random'] = $this->m_auction->getRandomAuctions();
			$this->load->view('header');
			$this->load->view('home', $data);
			$this->load->view('footer');
		}
		public function save_visitor()
		{
			$ip = $this->input->post('ip');
			$this->m_admin->save_visitor($ip);
		}
		
	}