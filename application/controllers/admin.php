<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class admin extends Admin_Controller {

		public function __construct() {
		parent::__construct();
		if($this->session->userdata('role') != 2){
				redirect(base_url());
		}
		$this->load->model('m_user');
		$this->load->model('m_admin');

    }
		public function index() {
			$data['visitors'] = $this->m_admin->get_visitors();
			$data['users'] = $this->m_admin->getAllUsers();
			$this->load->view('header');
			$this->load->view('admin/view', $data);
			$this->load->view('footer');
		}
		public function deleteUser() {
			$id = $this->input->post('id');
			$data = $this->m_admin->deleteUser($id);
			if($data){
				echo json_encode(array("status"=>'true'));
			}
			else{
				echo json_encode(array("status"=>'false'));
			}

		}
		public function updateUser(){
			$id = $this->input->post('id');
			$data = array(
				'fullname' => $this->input->post('fullname'),
				'bday' 		=> $this->input->post('bday'),
				'contact1' => $this->input->post('contact1'),
				'cc_code' => $this->input->post('cc_code'),
				'zip' => $this->input->post('zip'),
				'country' => $this->input->post('country'),
				'address'  => $this->input->post('address')
			);
			//echo $id;
			//print_r($data);
			$query = $this->m_user->updateUserInfo($data, $id);
			if($query){
				echo json_encode(array("status"=>'true'));
			}
			else{
				echo json_encode(array("status"=>'false'));
			}
			
		}
		public function check(){
			if($this->session->userdata('loggedin') != TRUE){
				redirect(base_url());
			}
		}
	}