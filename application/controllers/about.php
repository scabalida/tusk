<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class about extends Admin_Controller {
		public function __construct() {
		parent::__construct();

    }
		public function view() {
			$this->load->view('header');
			$this->load->view('about-us');
			$this->load->view('footer');
		}
		
	}