<?php defined('BASEPATH') OR exit('No direct script access allowed');
	define("BASE_URL", DIRECTORY_SEPARATOR . "tusk" . DIRECTORY_SEPARATOR);
	define("ROOT_PATH", $_SERVER['DOCUMENT_ROOT'] . BASE_URL);
	
	class auction extends Admin_Controller {
		public function __construct() {
		parent::__construct();

		$this->load->model('m_auction');
		$this->load->model('m_sold');
		$this->load->model('m_user');
    }
		public function index() {
			$data['items'] = $this->m_auction->getLatestAuctions();
			$this->load->view('header');
			$this->load->view('display_auctions', $data);
			$this->load->view('footer');
		}
		public function view($id){
			$data['auction'] = $this->m_auction->getAuctionData($id);
			$data['bidders'] = $this->m_auction->getTopFiveBidders($id);
			$this->load->view('header');
			$this->load->view('view_auction', $data);
			//$this->load->view('footer');
		}
		public function last_day() {
			$data['items'] = $this->m_auction->getLastDayAuction();
			$this->load->view('header');
			$this->load->view('soon-ending', $data);
			$this->load->view('footer');
		}
		public function categories($category) {
			$data['items'] = $this->m_auction->getAuctionsByCategory($category);
			$data['script_url'] = "js/auction.js";
			$this->load->view('header');
			$this->load->view('display_auctions', $data);
			$this->load->view('footer');
		}
		public function soon_ending($category) {
			$data['items'] = $this->m_auction->getLastAuctionByCategory($category);
			$this->load->view('header');
			$this->load->view('display_auctions', $data);
			$this->load->view('footer');
		}
		public function my_items() {
			$this->check();
			$data['items'] = $this->m_auction->getMyAuctionItems();
			$this->load->view('header');
			$this->load->view('auction-items', $data);
			$this->load->view('footer');
		}
		public function test_email() {
			$id = 26;
			$data = $this->m_auction->getAuctionDataWithBidders($id);
			print_r($data);
		}
		public function add() {
			$this->check();
			$this->load->view('header');
			$this->load->view('add_auction_item');
			$this->load->view('footer');
		}
		public function get(){
			
			$id = $this->input->post('id');
			$data = $this->m_auction->getAuctionDataWithBidders($id);

			echo json_encode($data);

		}
		public function getAuctionData(){
			
			$id = $this->input->post('id');
			$data = $this->m_auction->get($id);

			echo json_encode($data);

		}
		public function addItem(){
			$this->check();
			$id = $this->session->userdata('id');
			$filePath = "images/auctions/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/auctions/".$id;
			} else {
				mkdir("images/auctions/".$id);
				$config['upload_path']  =  "images/auctions/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('main_pic'))
            {
				$main_pic_img = $this->upload->data();
				$main_pic = $main_pic_img['file_name'];
			}else{
				$main_pic = "";
			}
			
			if ($this->upload->do_upload('other_pic1'))
            {
				$other_pic1_img = $this->upload->data();
				$other_pic1 = $other_pic1_img['file_name'];
			}else{
				$other_pic1 = "";
			}
			
			if ($this->upload->do_upload('other_pic2'))
            {
				$other_pic2_img = $this->upload->data();
				$other_pic2 = $other_pic2_img['file_name'];
			}else{
				$other_pic2 = "";
			}
			
			if ($this->upload->do_upload('other_pic3'))
            {
				$other_pic3_img = $this->upload->data();
				$other_pic3 = $other_pic3_img['file_name'];
			}else{
				$other_pic3 = "";
			}
			if($this->session->userdata('email') == "matras@live.dk" || $this->session->userdata('email') == "cabalida.stephen@gmail.com"){
				$featured = 1;
			}else{
				$featured = 0;
			}
			
			$data = array(
				'user_id' => $this->session->userdata('id'),
				'item_name' 		=> $this->input->post('item_name'),
				'item_desc' => $this->input->post('item_desc'),
				'min_price' => $this->input->post('min_price'),
				'designer_name' => $this->input->post('designer_name'),
				'category' => $this->input->post('category'),
				'location' => $this->input->post('item_loc'),
				'ship_to_international' => $this->input->post('ship_to_international'),
				'ship_to_local' => $this->input->post('ship_to_local'),
				'pick_up' => $this->input->post('pick_up'),
				'featured' => $featured,
				'main_pic'  => $main_pic,
				'other_pic1'  	 => $other_pic1,
				'other_pic2'  	 => $other_pic2,
				'other_pic3'  	 => $other_pic3,
				'status'  	 	=> 1,
				'date_added' =>   date("Y-m-d H:i:s"),
				'date_expired' =>  date('Y-m-d H:i:s', strtotime('+7 day'))
			);
			//print_r($data);
			$query = $this->m_auction->save($data);
			if($query){
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}
			
			
		}
		public function featuredPayment($id){
				$usr_pay = 20;
				//$config['business'] 			= 'tusk-test@gmail.com';
				$config['business'] 			= 'tusk.nu@gmail.com';
				$config['production'] 			= FALSE;
				$config['currency_code'] 		= "DKK";
				$config['custom'] 				= 4;
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url().'auction/items-list';
				$config['cancel_return'] 		= base_url().'auction/items-list';
				$config['notify_url'] 			= $notifyURL = base_url().'account/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("Featured Auction Payment",$usr_pay,1,$id);
				$this->paypal->pay();
			
		}
		public function renewItemPayment($id){
				$usr_pay = 30;
				//$config['business'] 			= 'tusk-test@gmail.com';
				$config['business'] 			= 'tusk.nu@gmail.com';
				$config['production'] 			= FALSE;
				$config['currency_code'] 		= "DKK";
				$config['custom'] 				= 5;
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url().'auction/items-list';
				$config['cancel_return'] 		= base_url().'auction/items-list';
				$config['notify_url'] 			= $notifyURL = base_url().'account/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("Renew Auction Payment",$usr_pay,1,$id);
				$this->paypal->pay();
			
		}
		public function soldAuctionPayment($id){
				$sold_data = $this->m_sold->get($id);
				$bid_price = $sold_data->bid_price;
				$usr_pay = $bid_price * 0.2;
				//$config['business'] 			= 'tusk-test@gmail.com';
				$config['business'] 			= 'tusk.nu@gmail.com';
				$config['production'] 			= FALSE;
				$config['currency_code'] 		= "DKK";
				$config['custom'] 				= 6;
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url().'auction/items-list';
				$config['cancel_return'] 		= base_url().'auction/items-list';
				$config['notify_url'] 			= $notifyURL = base_url().'account/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("Sold Auction Payment",$usr_pay,1,$id);
				$this->paypal->pay();
		}
		public function renewInstant(){
				$id = $this->input->post('id');
				$data = array(
					'date_expired' =>  date('Y-m-d H:i:s', strtotime('+7 day'))
				);
				
				$this->m_auction->save($data,$id);
				
				return true;
		}
		public function deleteItem() {
			$this->check();
			$id = $this->input->post('id');
			$item = $this->m_auction->get($id);
			
			$main_pic_name = $item->main_pic;
			$other_pic1_name = $item->other_pic1;
			$other_pic2_name = $item->other_pic2;
			$other_pic3_name = $item->other_pic3;
			
			$data = $this->m_auction->delete($id);

			//print_r($item);

				if($main_pic_name != NULL || $main_pic_name != "" || $main_pic_name != 0){
					$main_pic_path = "images/auctions/" . $this->session->userdata('id') . "/" . $main_pic_name;
					unlink(ROOT_PATH . $main_pic_path);
				}
				if($other_pic1_name != NULL || $other_pic1_name != "" || $other_pic1_name != 0){
					
					$other_pic1_path = "images/auctions/" . $this->session->userdata('id') . "/" . $other_pic1_name;
					unlink(ROOT_PATH . $other_pic1_path);
				}
				if($other_pic2_name != NULL || $other_pic2_name != "" || $other_pic2_name != 0){
					
					$other_pic2_path = "images/auctions/" . $this->session->userdata('id') . "/" . $other_pic2_name;
					unlink(ROOT_PATH . $other_pic2_path);
				}
				if($other_pic3_name != NULL || $other_pic3_name != "" || $other_pic3_name != 0){
					
					$other_pic3_path = "images/auctions/" . $this->session->userdata('id') . "/" . $other_pic3_name;
					unlink(ROOT_PATH . $other_pic3_path);
				}
				echo json_encode(array("status"=>'true'));

		}
		public function updateItem(){
			$this->check();
			$id = $this->session->userdata('id');

			$config['upload_path']  =  "images/auctions/".$id;

			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			$u_id = $this->input->post('id');
			$data['location'] = $this->input->post('item_loc');
			$data['ship_to_international'] = $this->input->post('ship_to_international');
			$data['ship_to_local'] = $this->input->post('ship_to_local');
			$data['pick_up'] = $this->input->post('pick_up');
			$data['item_name'] =  $this->input->post('item_name');
			$data['item_desc'] = $this->input->post('item_desc');
			$data['min_price'] =  $this->input->post('min_price');
			$data['designer_name'] = $this->input->post('designer_name');
			$data['category'] = $this->input->post('category');


			if ($this->upload->do_upload('main_pic'))
            {
				$main_pic_img = $this->upload->data();
				$main_pic = $main_pic_img['file_name'];
				$data['main_pic'] = $main_pic;
				$main_pic_path = "images/auctions/" . $this->session->userdata('id') . "/" . $this->input->post('old_main_pic');
				unlink(ROOT_PATH . $main_pic_path);
			}
			
			if ($this->upload->do_upload('other_pic1'))
            {
				$other_pic1_img = $this->upload->data();
				$other_pic1 = $other_pic1_img['file_name'];
				$data['other_pic1'] = $other_pic1;
				$other_pic1_path = "images/auctions/" . $this->session->userdata('id') . "/" . $this->input->post('old_other_pic1');
				unlink(ROOT_PATH . $other_pic1_path);
				
			}
			
			if ($this->upload->do_upload('other_pic2'))
            {
				$other_pic2_img = $this->upload->data();
				$other_pic2 = $other_pic2_img['file_name'];
				$data['other_pic2'] = $other_pic2;
				if($this->input->post('old_other_pic2') != NULL){
					$other_pic2_path = "images/auctions/" . $this->session->userdata('id') . "/" . $this->input->post('old_other_pic2');
					unlink(ROOT_PATH . $other_pic2_path);
				}
					
			}
			
			if ($this->upload->do_upload('other_pic3'))
            {
				$other_pic3_img = $this->upload->data();
				$other_pic3 = $other_pic3_img['file_name'];
				$data['other_pic3'] = $other_pic3;
				if($this->input->post('old_other_pic3') != NULL){
					$other_pic3_path = "images/auctions/" . $this->session->userdata('id') . "/" . $this->input->post('old_other_pic3');
					unlink(ROOT_PATH . $other_pic3_path);
				}
			}
			
			//print_r($data);
			$query = $this->m_auction->save($data,$u_id);
			if($query){
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}

		}
		public function soldNow(){
			
			$data = array(
				'seller_id' => $this->session->userdata('id'),
				'buyer_id' 		=> $this->input->post('bidder_id'),
				'auction_id' => $this->input->post('auction_id'),
				'bid_price' => $this->input->post('bidder_price'),
				'date_sold' =>   date("Y-m-d H:i:s")
			);
			
			$query = $this->m_sold->save($data);
			
			if($query){
				echo json_encode($query);
			}
			else{
				echo json_encode(0);
			}

		}
		public function getBuyerInfo($id){
			$data = $this->m_auction->getBuyerInfo($id);
			echo json_encode($data);

		}
		public function bought(){
			
			$data['items'] = $this->m_sold->getBoughtItems();
			$this->load->view('header');
			$this->load->view('bought_items', $data);
			$this->load->view('footer');	

		}
		public function renewItem(){
			
			$data = array(
				'date_expired' =>  date('Y-m-d H:i:s', strtotime('+7 day'))
			);
			$id = $this->input->post('id');
			$query = $this->m_auction->save($data,$id);

			if($query){
				echo json_encode("true");
			}
			else{
				echo json_encode("false");
			}

		}
		public function deleteImage(){
			
			$id = $this->input->post('id');
			$image_name = $this->input->post('image_name');
			$db_name = $this->input->post('db_name');
			$data = array(
				$db_name =>  ""
			);
			$path = "images/auctions/" . $this->session->userdata('id') . "/" . $image_name;
			unlink(ROOT_PATH . $path);
			$query = $this->m_auction->save($data,$id);

			if($query){
				echo json_encode("true");
			}
			else{
				echo json_encode("false");
			}

		}
		public function check(){
			if($this->session->userdata('loggedin') != TRUE){
				redirect(base_url());
			}
		}
		
	}