<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class account extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_user');
		$this->load->model('m_login');
		$this->load->model('m_auction');
		$this->load->model('m_sold');

    }
		public function index() {
			$this->check();
			$data['user_details'] = $this->m_user->getUserInfo();
			$this->load->view('header');
			$this->load->view('my-account',$data);
			$this->load->view('footer');
		}
		public function register() {
			$this->load->view('header');
			$this->load->view('user-register');
			$this->load->view('footer');
		}
		public function renew() {
			$this->load->view('header');
			$this->load->view('renew');
			$this->load->view('footer');
		}
		public function addUser(){
			$password = $this->input->post('password');
			$newPass = md5($password)."stekiesca";
			$data = array(
				'fullname' => $this->input->post('fullname'),
				'bday' 		=> $this->input->post('bday'),
				'contact1' => $this->input->post('contact1'),
				'cc_code' => $this->input->post('cc_code'),
				'zip' => $this->input->post('zip'),
				'country' => $this->input->post('country'),
				'address'  => $this->input->post('address'),
				'email'  	 => $this->input->post('email'),
				'password' => $newPass,
				'date_registered' =>   date("Y-m-d H:i:s"),
				'date_expired' =>  '0000-00-00',
				'verified' =>  0,
				'role' =>  1,
			);
			//print_r($data);
			$query = $this->m_user->save($data);
			if($query){
				echo json_encode(array("id"=>$query,"status"=>'true'));
			}
			else{
				echo json_encode(array("status"=>'false'));
			}
			
		}
		public function registerPayment($id){
				$usr_pay = 35;
				//$config['business'] 			= 'tusk-test@gmail.com';
				$config['business'] 			= 'tusk.nu@gmail.com';
				$config['production'] 			= FALSE;
				$config['currency_code'] 		= "DKK";
				$config['custom'] 				= 1;
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url();
				$config['cancel_return'] 		= base_url();
				$config['notify_url'] 			= $notifyURL = base_url().'account/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("Sign Up Payment",$usr_pay,1,$id);
				$this->paypal->pay();
			
		}
		public function renewAccount(){
				$id = $this->session->userdata('id');
				$usr_pay = 35;
				//$config['business'] 			= 'tusk-test@gmail.com';
				$config['business'] 			= 'tusk.nu@gmail.com';
				$config['production'] 			= FALSE;
				$config['currency_code'] 		= "DKK";
				$config['custom'] 				= 2;
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url().'account';
				$config['cancel_return'] 		= base_url().'account';
				$config['notify_url'] 			= $notifyURL = base_url().'account/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("Renew Account Payment",$usr_pay,1,$id);
				$this->paypal->pay();
			
		}
		public function renewAccount2(){
				$email = $this->input->post('email');
				$userInfo = $this->m_user->getUserID($email);
				$id = $userInfo[0]->id;
				$usr_pay = 35;
				//$config['business'] 			= 'tusk-test@gmail.com';
				$config['business'] 			= 'tusk.nu@gmail.com';
				$config['production'] 			= FALSE;
				$config['currency_code'] 		= "DKK";
				$config['custom'] 				= 3;
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url();
				$config['cancel_return'] 		= base_url();
				$config['notify_url'] 			= $notifyURL = base_url().'account/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("Renew Account Payment",$usr_pay,1,$id);
				$this->paypal->pay();
			
		}
		public function test(){
			$id = 1;
			$data = $this->m_sold->get($id);
			echo $data->buyer_id;
		}
		public function saveDB(){
			//paypal return transaction details array
			$paypalInfo	= $this->input->post();
			$checkStat = $paypalInfo['custom'];
			if($checkStat == 1){
				$id = $paypalInfo['item_number1'];
				$data = array(
					'date_expired' =>  date('Y-m-d H:i:s', strtotime('+1 year'))
				);
				
				$this->m_user->updateUserInfo($data, $id);
			}
			else if($checkStat == 2){
				$id = $paypalInfo['item_number1'];
				$getExpiredDate = $this->m_user->getUserExpired($id);
				$data = array(
					'date_expired' =>  date('Y-m-d H:i:s', strtotime('+1 year', strtotime($getExpiredDate[0]->date_expired)))
				);
				
				$this->m_user->updateUserInfo($data, $id);
			}
			else if($checkStat == 3){
				$id = $paypalInfo['item_number1'];
				$data = array(
					'date_expired' =>  date('Y-m-d H:i:s', strtotime('+1 year'))
				);
				
				$this->m_user->updateUserInfo($data, $id);
			}
			if($checkStat == 4){
				$id = $paypalInfo['item_number1'];
				$data = array(
					'featured' => 1
				);
				
				$this->m_auction->save($data,$id);
			}
			if($checkStat == 5){
				$id = $paypalInfo['item_number1'];
				$data = array(
					'date_expired' =>  date('Y-m-d H:i:s', strtotime('+7 day'))
				);
				
				$this->m_auction->save($data,$id);
			}
			if($checkStat == 6){
				$sold_id = $paypalInfo['item_number1'];
				$sold_arr = $this->m_sold->get($sold_id);
				$auction_id = $sold_arr->auction_id;
				$auction_data = array(
					'status' => 2
				);
				$sold_data = array(
					'payed' => 1
				);
				$this->m_auction->save($auction_data,$auction_id);
				$this->m_sold->save($sold_data,$sold_id);
				
				$bid_price = $sold_arr->bid_price;
				$seller_id = $sold_arr->seller_id;
				$buyer_id =  $sold_arr->buyer_id;
				$auction = $this->m_auction->getAuctionData($auction_id);
				$buyer = $this->m_user->getUserData($buyer_id);
				$seller = $this->m_user->getUserData($seller_id);
				$email_data = array(
					"item_name" => $auction[0]->item_name,
					"bid_price" => $bid_price,
					"main_pic" => $auction[0]->main_pic,
					"seller_id" => $seller_id,
					"seller_name" => ucwords($seller[0]->fullname),
					"seller_email" => $seller[0]->email,
					"seller_contact1" => $seller[0]->contact1,
					"seller_cc_code" => $seller[0]->cc_code,
					"seller_zip" => $seller[0]->zip,
					"seller_country" => $seller[0]->country,
					"buyer_name" => ucwords($buyer[0]->fullname),
					"buyer_email" => $buyer[0]->email,
					"buyer_contact1" => $buyer[0]->contact1,
					"buyer_cc_code" => $buyer[0]->cc_code,
					"buyer_zip" => $buyer[0]->zip,
					"buyer_country" => $buyer[0]->country,
				);
				
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
									
				$this->load->library('email',$config);
				$this->email->from("no-reply@tusk.nu", "Tusk");
				$this->email->to(array($buyer[0]->email, $seller[0]->email));
				$message = $this->load->view('sold_email',$email_data,true);

				$this->email->subject("AUCTION ITEM HAS BEEN SOLD");
				$this->email->message($message);
				$this->email->send();
			}
					
		}
		public function updateUser(){
			$this->check();
			$id = $this->input->post('id');
			$data = array(
				'fullname' => $this->input->post('fullname'),
				'bday' 		=> $this->input->post('bday'),
				'contact1' => $this->input->post('contact1'),
				'cc_code' => $this->input->post('cc_code'),
				'zip' => $this->input->post('zip'),
				'country' => $this->input->post('country'),
				'address'  => $this->input->post('address')
			);
			//echo $id;
			//print_r($data);
			$query = $this->m_user->updateUserInfo($data, $id);
			if($query){
				echo json_encode(array("status"=>'true'));
			}
			else{
				echo json_encode(array("status"=>'false'));
			}
			
		}
		public function updatePassword(){
			$this->check();
			$password = $this->input->post('password');
			$newPass = md5($password)."stekiesca";
			$id = $this->input->post('id');
			$data = array(
				'password' => $newPass
			);
			//echo $id;
			//print_r($data);
			$query = $this->m_user->updateUserInfo($data, $id);
			if($query){
				echo json_encode(array("status"=>'true'));
			}
			else{
				echo json_encode(array("status"=>'false'));
			}
			
		}
		public function logout()
		{
			$this->m_login->logout();
			
		}
		public function sendVerification(){
				$id = $this->session->userdata('id');
				$hash = "7d7edf4122b9d9be2d4af422a1817af3";
				$email = $this->session->userdata('email');
				$link = base_url()."account/verify/" . $hash . $id;
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from("support@tusk.nu", "Tusk Team");
				$this->email->to($email);
				$message = "<html><body><h3>Hi,<br><br>Click this link to verify your email.<br>".$link."<br><br>Thanks,<br>Tusk Team</h3></body></html>";
				//$message = "Verification Link:".$link;
				$this->email->subject("Email Verification");
				$this->email->message($message);
				$this->email->send();
			
		}

		public function verify($id){
			$hash = "7d7edf4122b9d9be2d4af422a1817af3";
			$getID = str_replace($hash,"",$id);
			$data = array(
				'verified' => 1
			);
			$query = $this->m_user->updateUserInfo($data, $getID);
			if($query){
				redirect(base_url());
			}
			
		}
		public function chckEmail(){
			$email = $this->input->post('email');
			$query = $this->m_user->chckEmail($email);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
		}
		public function edit() {
			$this->check();
			$data['user_details'] = $this->m_user->getUserInfo();
			$this->load->view('header');
			$this->load->view('edit-account', $data);
			$this->load->view('footer');
		}
		public function changepassword() {
			$this->load->view('header');
			$this->load->view('change-password');
			$this->load->view('footer');
		}
		public function check(){
			if($this->session->userdata('loggedin') != TRUE){
				redirect(base_url());
			}
		}
	}