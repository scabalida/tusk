<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class soon_ending extends Admin_Controller {
		public function __construct() {
		parent::__construct();

    }
		public function view() {
			$this->load->view('header');
			$this->load->view('soon-ending');
			$this->load->view('footer');
		}
		
	}