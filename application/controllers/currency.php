<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class currency extends Admin_Controller {
		public function __construct() {
		parent::__construct();

    }
		public function usd($base_price) {
			$req_url = 'https://api.exchangerate-api.com/v4/latest/USD';
			$response_json = file_get_contents($req_url);
			if(false !== $response_json) {
				try {
					$response_object = json_decode($response_json);
					$EUR_price = round(($base_price * $response_object->rates->EUR), 2);
					return $EUR_price;
				}
				catch(Exception $e) {
				}
			}
		}
		
	}