<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class bid extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		if($this->session->userdata('loggedin') != TRUE){
				redirect(base_url());
			}
		$this->load->model('m_bidders');
    }
		public function index() {
		}
		public function add(){
			
			$data = array(
				'bidder_id' => $this->session->userdata('id'),
				'auction_id' 		=> $this->input->post('auction_id'),
				'bidder_price' => $this->input->post('bidder_price'),
				'date_bid' =>   date("Y-m-d H:i:s")
			);
			//print_r($data);
			$query = $this->m_bidders->save($data);

			if($query){
				echo json_encode("true");
			}
			else{
				echo json_encode("false");
			}

		}
		public function getBidders(){
			
			$id = $this->input->post('id');
			$data = $this->m_bidders->getBidders($id);

			echo json_encode($data);

		}
	}