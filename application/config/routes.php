<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['about-us'] = "about/view";
$route['contact-us'] = "contact/view";
$route['auction/items-list'] = "auction/my_items";
$route['soon-ending'] = "auction/last_day";
$route['auction/categories/carpets-rugs-and-textiles'] = "auction/categories/1";
$route['auction/categories/clothing-shoes-and-accessories'] = "auction/categories/2";
$route['auction/categories/electronics'] = "auction/categories/3";
$route['auction/categories/furniture'] = "auction/categories/4";
$route['auction/categories/glass-porcelain-and-ceramics'] = "auction/categories/5";
$route['auction/categories/hobby-and-collectibles'] = "auction/categories/6";
$route['auction/categories/home-and-garden'] = "auction/categories/7";
$route['auction/categories/hunting-fishing-arms-and-militaria'] = "auction/categories/8";
$route['auction/categories/jewellery'] = "auction/categories/9";
$route['auction/categories/lamps-and-lighting'] = "auction/categories/10";
$route['auction/categories/paintings-and-sculptures'] = "auction/categories/11";
$route['auction/categories/silver-bronze-copper-and-pewter'] = "auction/categories/12";
$route['auction/categories/sport-and-leisure'] = "auction/categories/13";
$route['auction/categories/travel-and-experiences'] = "auction/categories/14";
$route['auction/categories/watches-and-clocks'] = "auction/categories/15";
$route['auction/categories/wine-and-spirits'] = "auction/categories/16";

$route['auction/soon-ending/carpets-rugs-and-textiles'] = "auction/soon_ending/1";
$route['auction/soon-ending/clothing-shoes-and-accessories'] = "auction/soon_ending/2";
$route['auction/soon-ending/electronics'] = "auction/soon_ending/3";
$route['auction/soon-ending/furniture'] = "auction/soon_ending/4";
$route['auction/soon-ending/glass-porcelain-and-ceramics'] = "auction/soon_ending/5";
$route['auction/soon-ending/hobby-and-collectibles'] = "auction/soon_ending/6";
$route['auction/soon-ending/home-and-garden'] = "auction/soon_ending/7";
$route['auction/soon-ending/hunting-fishing-arms-and-militaria'] = "auction/soon_ending/8";
$route['auction/soon-ending/jewellery'] = "auction/soon_ending/9";
$route['auction/soon-ending/lamps-and-lighting'] = "auction/soon_ending/10";
$route['auction/soon-ending/paintings-and-sculptures'] = "auction/soon_ending/11";
$route['auction/soon-ending/silver-bronze-copper-and-pewter'] = "auction/soon_ending/12";
$route['auction/soon-ending/sport-and-leisure'] = "auction/soon_ending/13";
$route['auction/soon-ending/travel-and-experiences'] = "auction/soon_ending/14";
$route['auction/soon-ending/watches-and-clocks'] = "auction/soon_ending/15";
$route['auction/soon-ending/wine-and-spirits'] = "auction/soon_ending/16";

/* End of file routes.php */
/* Location: ./application/config/routes.php */