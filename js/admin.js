(function() {
	'use strict';
	$('#allUsersTable').DataTable( {
		"lengthChange": false,
		"ordering": false,
		"pageLength": 10,
		"responsive": true
	} );

})();
function delete_user_form(id){
			$.ajax({
				type: "POST",
				url: "admin/deleteUser",
				dataType: "json",
				data:  {id:id} ,
					success:
						function(data) {
							//console.log(data);
							if(data.status == "true"){
								$('#deleteUserSuccess').show(); 
								$('#deleteUserError').hide();
								$("#deleteUserModal" + id).modal("hide");
								setTimeout(function(){ 
									$('#deleteUserSuccess').hide();
									window.location.reload();
								}, 2000);	
							}else{
								$('#deleteUserSuccess').hide(); 
								$('#deleteUserError').show();
							}
							
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
							$('#deleteUserError').show();									
						}
			});
			return false;
}
function update_user_form(id){
		var fullname = $("#fullname"+ id).val();
		var bday = $("#bday"+ id).val();
		var cc_code = $("#cc_code"+ id).val();
		var zip = $("#zip"+ id).val();
		var country = $("#country"+ id).val();
		var address = $("#address"+ id).val();
		var contact1 = $("#contact1"+ id).val();
			$.ajax({
				type: "POST",
				url: "admin/updateUser",
				dataType: "json",
				data:{id:id,fullname:fullname,bday:bday,address:address,contact1:contact1,cc_code:cc_code,zip:zip,country:country},
					success:
						function(data) {
							//console.log(data);
							if(data.status == "true"){
								$('#updateUserSuccess').show(); 
								$('#updateUserError').hide();
								$("#updateUserModal" + id).modal("hide");
								setTimeout(function(){ 
									$('#updateUserSuccess').hide();
									window.location.reload();
								}, 2000);	
							}else{
								$('#updateUserSuccess').hide(); 
								$('#updateUserError').show();
							}
							
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
							$('#updateUserError').show();									
						}
			});
			return false;
}
function userType(d){
	if(d == 1){
		return "Regular";
	}else if(d == 2){
		return "Donator";
	}else if(d == 3){
		return "VIP";
	}else if(d == 4){
		return "Updater";
	}else if(d == 5){
		return "Operator";
	}else if(d == 6){
		return "Admin";
	}else{
		return "Regular";
	}
	
}

function getMonthDesc(m){
	var mDesc = "";
	switch (m){
		case 1:
		mDesc = "January";
		break;
		case 2:
		mDesc = "February";
		break;
		case 3:
		mDesc = "March";
		break;
		case 4:
		mDesc = "April";
		break;
		case 5:
		mDesc = "May";
		break;
		case 6:
		mDesc = "June";
		break;
		case 7:
		mDesc = "July";
		break;
		case 8:
		mDesc = "August";
		break;
		case 9:
		mDesc = "September";
		break;
		case 10:
		mDesc = "October";
		break;
		case 11:
		mDesc = "November";
		break;
		case 12:
		mDesc = "December";
		break;
	}
	return mDesc;
}
function reformatDate(d){
	var dbDate = new Date(d);
	var yyyy = dbDate.getFullYear();
	var mm = dbDate.getMonth() + 1;
	var dd = dbDate.getDate();
	return getMonthDesc(mm)+" "+dd+", "+yyyy;
}

function setDate(n){
	var d = new Date(n);
	
	var month = d.getMonth();
	var mWord;
	switch(month){
		case 0:
        mWord = "January";
        break;
    case 1:
        mWord = "February";
        break;
	case 2:
        mWord = "March";
        break;
	case 3:
        mWord = "April";
        break;
	case 4:
        mWord = "May";
        break;
	case 5:
        mWord = "June";
        break;
	case 6:
        mWord = "July";
        break;
	case 7:
        mWord = "August";
        break;
	case 8:
        mWord = "September";
        break;
	case 9:
        mWord = "October";
        break;
	case 10:
        mWord = "November";
        break;
	case 11:
        mWord = "December";
        break;
	}
	var nDate = d.getDate();
	var nYear = d.getFullYear();
	
	return mWord+" "+nDate+", "+nYear;
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}
