var g_id = 0;
var auction_id = 0;
var bidder_id = 0;
var bidder_price = 0;

var id = 0;
var image_name = 0;
var db_name = 0;

var featured_id = 0;
var s_user_id, s_id, s_pic, s_name, s_desc;
(function() {
	'use strict';
	$('#allMyAuctionsTable').DataTable( {
		"lengthChange": false,
		"ordering": false,
		"pageLength": 10,
		"responsive": true
	} );
	$('#boughtItemsTable').DataTable( {
		"lengthChange": false,
		"ordering": false,
		"pageLength": 10,
		"responsive": true
	} );
	$("#ship_to_international").change(function(){
		if($(this).is(':checked') == true){
			$(this).val(1);
		}else{
			$(this).val(0);
		}
	});
	$("#ship_to_local").change(function(){
		if($(this).is(':checked') == true){
			$(this).val(1);
		}else{
			$(this).val(0);
		}
	});
	$("#pick_up").change(function(){
		if($(this).is(':checked') == true){
			$(this).val(1);
		}else{
			$(this).val(0);
		}
	});
	
	$("#ship_to_international_u").change(function(){
		if($(this).is(':checked') == true){
			$(this).val(1);
		}else{
			$(this).val(0);
		}
	});
	$("#ship_to_local_u").change(function(){
		if($(this).is(':checked') == true){
			$(this).val(1);
		}else{
			$(this).val(0);
		}
	});
	$("#pick_up_u").change(function(){
		if($(this).is(':checked') == true){
			$(this).val(1);
		}else{
			$(this).val(0);
		}
	});
	$('#add-auction-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();

			var passdata = new FormData(this);
				passdata.append('ship_to_international', $("#ship_to_international").val());
				passdata.append('ship_to_local', $("#ship_to_local").val());
				passdata.append('pick_up', $("#pick_up").val());
				passdata.append('item_loc', $("#item_loc").val());
				passdata.append('item_name', $("#item_name").val());
				passdata.append('item_desc', $("#item_desc").val());
				passdata.append('featured', $("#featured").val());
				passdata.append('min_price', $("#min_price").val());
				passdata.append('designer_name', $("#designer_name").val());
				passdata.append('category', $("#category").val());
			$.ajax({
				type: "POST",
				url: "addItem",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						//console.log(data);
						if(data == "true"){
							//console.log(1);
							$('#addAuctionItemSuccess').show(); 
							$('#addAuctionItemError').hide(); 
							$('#ship_to_international').prop('checked', false);
							$('#ship_to_local').prop('checked', false);
							$('#pick_up').prop('checked', false);
							$("#item_name").val("");
							$("#item_loc").val("");
							$("#item_desc").val("");
							$("#min_price").val("");
							$("#designer_name").val("");
							$("#main_pic").val("");
							$("#other_pic1").val("");
							$("#other_pic2").val("");
							$("#other_pic3").val("");
							setTimeout(function(){$('#addAuctionItemSuccess').hide();}, 2000);	
						}else{
							//console.log(2);
							$('#addAuctionItemSuccess').hide(); 
							$('#addAuctionItemError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(3);
					//console.log(data);
					$('#addAuctionItemSuccess').hide(); 
					$('#addAuctionItemError').show(); 	
				}
			});
		}
	});	
	$('#featuredNow').click(function(e){
			e.preventDefault();
			window.location.href = "http://tusk.nu/auction/featuredPayment/"+featured_id;
	});
	$('#update-auction-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();

			var passdata = new FormData(this);
				passdata.append('id', $("#item_id").val());
				passdata.append('ship_to_international', $("#ship_to_international_u").val());
				passdata.append('ship_to_local', $("#ship_to_local_u").val());
				passdata.append('pick_up', $("#pick_up_u").val());
				passdata.append('item_loc', $("#item_loc").val());
				passdata.append('item_name', $("#item_name").val());
				passdata.append('item_desc', $("#item_desc").val());
				passdata.append('min_price', $("#min_price").val());
				passdata.append('designer_name', $("#designer_name").val());
				passdata.append('category', $("#category").val());
				passdata.append('old_main_pic', $("#old_main_pic").val());
				passdata.append('old_other_pic1', $("#old_other_pic1").val());
				passdata.append('old_other_pic2', $("#old_other_pic2").val());
				passdata.append('old_other_pic3', $("#old_other_pic3").val());
			$.ajax({
				type: "POST",
				url: "updateItem",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						//console.log(data);
						if(data == "true"){
							//console.log(1);
							$('#updateAuctionItemSuccess').show(); 
							$('#updateAuctionItemError').hide(); 
							$('#ship_to_international').prop('checked', false);
							$('#ship_to_local').prop('checked', false);
							$('#pick_up').prop('checked', false);
							$("#item_name").val("");
							$("#item_loc").val("");
							$("#item_desc").val("");
							$("#min_price").val("");
							$("#designer_name").val("");
							$("#main_pic").val("");
							$("#other_pic1").val("");
							$("#other_pic2").val("");
							$("#other_pic3").val("");
							$("#updateAuctionModal").modal("hide");
							setTimeout(function(){ window.location.reload(); }, 2000);	
						}else{
							//console.log(2);
							$('#updateAuctionItemSuccess').hide(); 
							$('#updateAuctionItemError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(3);
					//console.log(data);
					$('#updateAuctionItemSuccess').hide(); 
					$('#updateAuctionItemError').show(); 	
				}
			});
		}
	});	
	$('#send-bid-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(1);
		} else {
			//console.log(0);
			e.preventDefault();
			if($("#bidder_price").val() >= $("#auction_min_price").val()){
				//console.log(2);
				$('#lowBid').hide(); 
				$.ajax({
					type: "POST",
					url: "bid/add",
					dataType: "json",
					data:  $(this).serialize() ,
						success:
							function(data) {
								//console.log(data);
								if(data == "true"){
									$('#addBidSuccess').show(); 
									$('#addBidError').hide();
									$("#bidder_price").val("");
									setTimeout(function(){ $('#addBidSuccess').hide();}, 2200);	
								}else{
									$('#addBidSuccess').hide(); 
									$('#addBidError').show();
								}
							},
						error:
							function(data){
								//console.log(data);		
								//console.log("false");
								$('#addBidSuccess').hide();									
								$('#addBidError').show();									
							}
				});
			}else{
				$('#lowBid').show(); 
			}
		}
	});
	$('#soldNow').click(function(e){
			e.preventDefault();
			//console.log(bidder_price);
			//console.log(auction_id);
			//console.log(bidder_id);
				$.ajax({
					type: "POST",
					url: "soldNow",
					dataType: "json",
					data:  {bidder_price:bidder_price,auction_id:auction_id,bidder_id:bidder_id} ,
						success:
							function(data) {
								//console.log(data);
								$('#confirmSoldItem').modal("hide"); 
								$('#displayBids').modal("hide"); 
								if(data > 0){
									window.location.href = "http://tusk.nu/auction/soldAuctionPayment/"+data;
									//$('#soldAuctionItemSuccess').show(); 
									//$('#soldAuctionItemError').hide();
									//setTimeout(function(){ $('#soldAuctionItemSuccess').hide();}, 2200);	
								}else{
									$('#soldAuctionItemSuccess').hide(); 
									$('#soldAuctionItemError').show();
								}
							},
						error:
							function(data){
								$('#soldAuctionItemSuccess').hide();									
								$('#soldAuctionItemError').show();									
							}
				});
	});
	$('#deletePic1').click(function(e){
			e.preventDefault();
			id = $(this).attr("data-id");
			image_name = $(this).attr("data-name");
			db_name = $(this).attr("data-db");
			$("#deletePicModal").modal("show");
	});
	$('#deletePic2').click(function(e){
			e.preventDefault();
			id = $(this).attr("data-id");
			image_name = $(this).attr("data-name");
			db_name = $(this).attr("data-db");
			$("#deletePicModal").modal("show");
	});
	$('#deletePic3').click(function(e){
			e.preventDefault();
			id = $(this).attr("data-id");
			image_name = $(this).attr("data-name");
			db_name = $(this).attr("data-db");
			$("#deletePicModal").modal("show");
	});
	$('#deletePic4').click(function(e){
			e.preventDefault();
			id = $(this).attr("data-id");
			image_name = $(this).attr("data-name");
			db_name = $(this).attr("data-db");
			$("#deletePicModal").modal("show");
	});
	$('#deletePic').click(function(e){
			e.preventDefault();
			//console.log(id);
			//console.log(image_name);
			//console.log(db_name);
				$.ajax({
					type: "POST",
					url: "deleteImage",
					dataType: "json",
					data:  {id:id,image_name:image_name,db_name:db_name} ,
						success:
							function(data) {
								//console.log(data);
								
								if(data == "true"){
									$('#imageDeleteItemSuccess').show(); 
									$('#imageDeleteItemError').hide();
									setTimeout(function(){ $('#imageDeleteItemSuccess').hide();}, 2200);	
								}else{
									$('#imageDeleteItemSuccess').hide(); 
									$('#imageDeleteItemError').show();
								}
								$('#deletePicModal').modal("hide"); 
								$('#updateAuctionModal').modal("hide"); 
							},
						error:
							function(data){
								$('#imageDeleteItemSuccess').hide();									
								$('#imageDeleteItemError').show();									
							}
				});
	});
	$('.btn_count').click();	
})();
function showCountdown(time,id){
	//console.log(time);
	//console.log(id);
	var clock;

	clock = $('#btn_count'+id).FlipClock({
		clockFace: 'DailyCounter',
		autoStart: false,
	});
				    
	clock.setTime(time);
	clock.setCountdown(true);
	clock.start();
}
function show_update_modal(id){
	g_id = id;
	//console.log(g_id);
	$("#updateAuctionModal"+id).modal("show");
}

function featuredThis(id){
	featured_id = id;
	//console.log(g_id);
	$("#featuredItemModal").modal("show");
}
function delete_auction_form(id){
			$.ajax({
				type: "POST",
				url: "deleteItem",
				dataType: "json",
				data:  {id:id} ,
					success:
						function(data) {
							//console.log(data);
							if(data.status == "true"){
								$('#renewAuctionItemSuccess').show(); 
								$('#renewAuctionItemError').hide();
								$("#deleteAuctionModal" + id).modal("hide");
								setTimeout(function(){ 
									$('#renewAuctionItemSuccess').hide();
									window.location.reload();
								}, 2000);	
							}else{
								$('#renewAuctionItemSuccess').hide(); 
								$('#renewAuctionItemError').show();
							}
							
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
							$('#renewAuctionItemError').show();									
						}
			});
			return false;
}
function renew_auction_form(id){
		/*	$.ajax({
				type: "POST",
				url: "renewItem",
				dataType: "json",
				data:  {id:id} ,
					success:
						function(data) {
							//console.log(data);
							if(data == "true"){
								$('#renewAuctionItemSuccess').show(); 
								$('#renewAuctionItemError').hide();
								$("#renewAuctionModal" + id).modal("hide");
								setTimeout(function(){ 
									$('#renewAuctionItemSuccess').hide();
									window.location.reload();
								}, 2000);	
							}else{
								$('#renewAuctionItemSuccess').hide(); 
								$('#renewAuctionItemError').show();
							}
							
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
							$('#renewAuctionItemError').show();									
						}
			});
			*/
		window.location.href = "http://tusk.nu/auction/renewItemPayment/"+id;
		return false;
}
function renew_instant(id){
			$.ajax({
				type: "POST",
				url: "renewInstant",
				dataType: "json",
				data:  {id:id} ,
					success:
						function(data) {
							//console.log(data);
							if(data == "true"){
								$('#renewAuctionItemSuccess').show(); 
								$('#renewAuctionItemError').hide();
								$("#renewAuctionModal" + id).modal("hide");
								setTimeout(function(){ 
									$('#renewAuctionItemSuccess').hide();
									window.location.reload();
								}, 2000);	
							}else{
								$('#renewAuctionItemSuccess').hide(); 
								$('#renewAuctionItemError').show();
							}
							
						},
					error:
						function(data){
							$('#renewAuctionItemError').show();									
						}
			});
	return false;
}
function showUpdateAuction(id){
		$("#pic-1").hide();
		$("#pic-2").hide();
		$("#pic-3").hide();
		$("#pic-4").hide();
		document.getElementById('pic-img-1').src = "";
		document.getElementById('pic-img-2').src = "";
		document.getElementById('pic-img-3').src = "";
		document.getElementById('pic-img-4').src = "";
			$.ajax({
				type: "POST",
				url: "getAuctionData",
				dataType: "json",
				data:  {id:id} ,
					success:
						function(data) {
							//console.log(data);
							var base_url = $("#base_url").val();
							$("#item_id").val(data.u_id);
							$("#item_name").val(data.item_name);
							$("#min_price").val(data.min_price);
							$("#designer_name").val(data.designer_name);
							$("#item_desc").val(data.item_desc);
							$("#item_loc").val(data.location);
							$("#old_main_pic").val(data.main_pic);
							$("#old_other_pic1").val(data.other_pic1);
							$("#old_other_pic2").val(data.other_pic2);
							$("#old_other_pic3").val(data.other_pic3);
							document.getElementById('category').value = data.category;
							
							if(data.ship_to_international == 1){
								$('#ship_to_international_u').val(1);
								$('#ship_to_international_u').prop('checked', true);
							}
							if(data.ship_to_local == 1){
								$('#ship_to_local_u').val(1);
								$('#ship_to_local_u').prop('checked', true);
							}
							if(data.pick_up == 1){
								$('#pick_up_u').val(1);
								$('#pick_up_u').prop('checked', true);
							}
							if(data.main_pic != ""){
								document.getElementById('pic-img-1').src = base_url +"images/auctions/"+data.user_id+"/"+data.main_pic;
								$("#deletePic1").attr("data-id",data.u_id);
								$("#deletePic1").attr("data-name",data.main_pic);
								$("#deletePic1").attr("data-db","main_pic");
								$("#pic-1").show();
							}
							if(data.other_pic1 != ""){
								document.getElementById('pic-img-2').src = base_url +"images/auctions/"+data.user_id+"/"+data.other_pic1;
								$("#deletePic2").attr("data-id",data.u_id);
								$("#deletePic2").attr("data-name",data.other_pic1);
								$("#deletePic2").attr("data-db","other_pic1");
								$("#pic-2").show();
							}
							if(data.other_pic2 != ""){
								document.getElementById('pic-img-3').src = base_url +"images/auctions/"+data.user_id+"/"+data.other_pic2;
								$("#deletePic3").attr("data-id",data.u_id);
								$("#deletePic3").attr("data-name",data.other_pic2);
								$("#deletePic3").attr("data-db","other_pic2");
								$("#pic-3").show();
							}
							if(data.other_pic3 != ""){
								document.getElementById('pic-img-4').src = base_url +"images/auctions/"+data.user_id+"/"+data.other_pic3;
								$("#deletePic4").attr("data-id",data.u_id);
								$("#deletePic4").attr("data-name",data.other_pic3);
								$("#deletePic4").attr("data-db","other_pic3");
								$("#pic-4").show();
							}
							$("#updateAuctionModal").modal("show");
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");									
						}
			});
			return false;
}
function showAuctionDetails(timer,id){
		var URL = "";
		var href = window.location.href;
		var hostname = window.location.hostname;
		//console.log(href);
		//console.log(hostname + "/auction");
		$("#pic-1").hide();
		$("#pic-2").hide();
		$("#pic-3").hide();
		$("#pic-4").hide();
		document.getElementById('pic-img-1').src = "";
		document.getElementById('pic-img-2').src = "";
		document.getElementById('pic-img-3').src = "";
		document.getElementById('pic-img-4').src = "";
		if(href == hostname + "/auction" || href == hostname + "/auction#"){
			URL = "auction/get";
			//console.log(URL);
		}else{
			URL = "../../auction/get";
			//console.log(URL);
		}
		
			$.ajax({
				type: "POST",
				url: URL,
				dataType: "json",
				data:  {id:id} ,
					success:
						function(data) {
							//console.log(data);
							var base_url = $("#base_url").val();
							$("#auction_id").val(data[0].u_id);
							$("#item_id").val(data[0].u_id);
							$("#auc_name").html(data[0].item_name);
							$("#auction_min_price").val(data[0].min_price);
							$("#auc_price").html(data[0].min_price);
							if(data[0].designer_name != ""){
								$("#auc_designer_label").show();
								$("#auc_designer").html(data[0].designer_name);
							}else{
								$("#auc_designer_label").hide();
							}
							
							s_id = data[0].u_id;
							s_user_id = data[0].user_id;
							s_name = data[0].item_name;
							s_desc = data[0].item_desc;
							s_pic = data[0].main_pic;
							
							$("#auc_desc").html(data[0].item_desc);
							$("#auc_loc").html(data[0].location);
							
							if(data[0].ship_to_international == 1){
								$("#ship_i").show();
							}else{
								$("#ship_i").hide();
							}
							
							if(data[0].ship_to_local == 1){
								$("#ship_l").show();
							}else{
								$("#ship_l").hide();
							}
							
							if(data[0].pick_up == 1){
								$("#pick_u").show();
							}else{
								$("#pick_u").hide();
							}
							
							if(data[0].main_pic != ""){
								document.getElementById('pic-img-1').src = base_url +"images/auctions/"+data[0].user_id+"/"+data[0].main_pic;
								$("#pic-1").show();
							}
							if(data[0].other_pic1 != ""){
								document.getElementById('pic-img-2').src = base_url +"images/auctions/"+data[0].user_id+"/"+data[0].other_pic1;
								$("#pic-2").show();
							}
							if(data[0].other_pic2 != ""){
								document.getElementById('pic-img-3').src = base_url +"images/auctions/"+data[0].user_id+"/"+data[0].other_pic2;
								$("#pic-3").show();
							}
							if(data[0].other_pic3 != ""){
								document.getElementById('pic-img-4').src = base_url +"images/auctions/"+data[0].user_id+"/"+data[0].other_pic3;
								$("#pic-4").show();
							}
							
							if(data[1].length > 0){
								$("#tbodyTopFiveBid").empty();
								for(var x=0;x < data[1].length;x++){
									$("#topFiveBid").find('tbody')
										.append($('<tr>')
											.append($('<td>')
												.append(zeroPad(data[1][x].bidder_id , 10))
											)
											.append($('<td>')
												.append(data[1][x].bidder_price)
											)
										);
								}
								$("#topFiveBid").show();
								$("#notopfivebid").hide();
							}else{
								$("#topFiveBid").hide();
								$("#notopfivebid").show();
							}
							var clock;

							clock = $('#view_auction_clock').FlipClock({
								clockFace: 'DailyCounter',
								autoStart: false,
							});
											
							clock.setTime(timer);
							clock.setCountdown(true);
							clock.start();
							$("#showMore").modal("show");
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");									
						}
			});
			return false;
}
function shareNow(){
     var share = {
            method: 'stream.share',
            quote: 'Auction Name: '+s_name+' \n Description: '+s_desc+'\n Picture: https://tusk.nu/images/auctions/'+s_user_id+'/'+s_pic,
            u: 'https://tusk.nu/auction/view/'+s_id
     };
 
     FB.ui(share, function(response) { console.log(response); });
}
function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}
function showBidders(id){
	
	var myBiddersTable = $('#myBiddersTable').DataTable();
	var base_url = $("#baseurl").val();
	myBiddersTable.destroy();
   $.ajax({
		type: "POST",
		url: "../bid/getBidders",
		dataType: "json",
		data:{id:id},
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyMyBidders").empty();
					for(var x=0;x < data.length;x++){
						$("#myBiddersTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append($('<div>')
										.attr('class', "row")
										.append($('<div>')
											.attr('class', "col-md-6 col-sm-6 text-left")
											.append($('<h5>')
												.append($('<strong>')
													.append(data[x].fullname)
												)
											)
										)
										.append($('<div>')
											.attr('class', "col-md-6 col-sm-6 text-right")
											.append($('<h4>')
												.append("$")
												.append($('<strong>')
													.attr('class', "text-danger")
													.append(data[x].bidder_price)
												)
											)
											
												.append($('<input>')
													.attr('type', "hidden")
													.attr('id', "auction_id"+data[x].u_id)
													.attr('name', "auction_id"+data[x].u_id)
													.attr('value', data[x].auction_id)
												)
												.append($('<input>')
													.attr('type', "hidden")
													.attr('id', "bidder_id"+data[x].u_id)
													.attr('name', "bidder_id"+data[x].u_id)
													.attr('value', data[x].bidder_id)
												)
												.append($('<input>')
													.attr('type', "hidden")
													.attr('id', "bidder_price"+data[x].u_id)
													.attr('name', "bidder_price"+data[x].u_id)
													.attr('value', data[x].bidder_price)
												)
												.append($('<button>')
													.attr('onclick', "confirmSold("+data[x].u_id+")")
													.attr('class', "btn btn-danger sold-btn")
													.append("SOLD TO "+data[x].fullname)
												)
											
										)
									)
								)
							);
					}
					$("#myBiddersTable").show();
					$("#nobidders").hide();
					myBiddersTable = $('#myBiddersTable').DataTable( {
						"searching": false,
						"lengthChange": false,
						"ordering": false,
						"pageLength": 10
						});
					
				}else{
					$("#myBiddersTable").hide();
					$("#nobidders").show();
				}
				$("#displayBids").modal("show");
			},
		error:
		function(data){
			//console.log("false");		
		}
	});	
}
function confirmSold(id){
	$("#confirmSoldItem").modal("show");
	auction_id = $("#auction_id"+id).val();
	bidder_id = $("#bidder_id"+id).val();
	bidder_price = $("#bidder_price"+id).val();
}
function showBuyerInfo(id){
	$.ajax({
		type: "GET",
		url: "getBuyerInfo/"+id,
		dataType: "json",
		success:
			function(data) {
				//console.log(data);
				$("#showBuyerName").html(capitalizeFirstLetter(data[0].fullname));
				$("#showBuyerEmail").html(data[0].email);
				$("#showBuyerAddress").html(data[0].address);
				$("#showBuyerZip").html(data[0].zip);
				$("#showBuyerCountry").html(data[0].country);
				$("#showBuyerContact").html("("+data[0].cc_code+") "+ data[0].contact1);
				$("#displayBuyerInfo").modal("show");				
			},
		error:
			function(data){									
			}
	});
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function userType(d){
	if(d == 1){
		return "Regular";
	}else if(d == 2){
		return "Donator";
	}else if(d == 3){
		return "VIP";
	}else if(d == 4){
		return "Updater";
	}else if(d == 5){
		return "Operator";
	}else if(d == 6){
		return "Admin";
	}else{
		return "Regular";
	}
	
}

function getMonthDesc(m){
	var mDesc = "";
	switch (m){
		case 1:
		mDesc = "January";
		break;
		case 2:
		mDesc = "February";
		break;
		case 3:
		mDesc = "March";
		break;
		case 4:
		mDesc = "April";
		break;
		case 5:
		mDesc = "May";
		break;
		case 6:
		mDesc = "June";
		break;
		case 7:
		mDesc = "July";
		break;
		case 8:
		mDesc = "August";
		break;
		case 9:
		mDesc = "September";
		break;
		case 10:
		mDesc = "October";
		break;
		case 11:
		mDesc = "November";
		break;
		case 12:
		mDesc = "December";
		break;
	}
	return mDesc;
}
function reformatDate(d){
	var dbDate = new Date(d);
	var yyyy = dbDate.getFullYear();
	var mm = dbDate.getMonth() + 1;
	var dd = dbDate.getDate();
	return getMonthDesc(mm)+" "+dd+", "+yyyy;
}

function setDate(n){
	var d = new Date(n);
	
	var month = d.getMonth();
	var mWord;
	switch(month){
		case 0:
        mWord = "January";
        break;
    case 1:
        mWord = "February";
        break;
	case 2:
        mWord = "March";
        break;
	case 3:
        mWord = "April";
        break;
	case 4:
        mWord = "May";
        break;
	case 5:
        mWord = "June";
        break;
	case 6:
        mWord = "July";
        break;
	case 7:
        mWord = "August";
        break;
	case 8:
        mWord = "September";
        break;
	case 9:
        mWord = "October";
        break;
	case 10:
        mWord = "November";
        break;
	case 11:
        mWord = "December";
        break;
	}
	var nDate = d.getDate();
	var nYear = d.getFullYear();
	
	return mWord+" "+nDate+", "+nYear;
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}
