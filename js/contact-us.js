(function() {
	'use strict';
	$('#sendMailForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(1);
		} else {
			//console.log(0);
			e.preventDefault();
			$.ajax({
				type: "POST",
				url: "contact/sendEmail",
				dataType: "json",
				data:  $(this).serialize() ,
					success:
						function(data) {
							console.log(data);
							if(data == "true"){
								$('#sendMailSuccess').show(); 
								$('#sendMailError').hide();
								$("#e_name").val("");
								$("#e_email").val("");
								$("#e_subject").val("");
								$("#e_message").val("");
								setTimeout(function(){ $('#sendMailSuccess').hide();}, 2200);	
							}else{
								$('#sendMailSuccess').hide(); 
								$('#sendMailError').show();
							}
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
							$('#addMailError').show();									
						}
			});
		}
	});
})();