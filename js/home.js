var user_id,id,pic,name,desc;
(function() {
	'use strict';
	saveVisitor();
	$('#send-bid-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(1);
		} else {
			//console.log(0);
			e.preventDefault();
			if($("#bidder_price").val() >= $("#auction_min_price").val()){
				//console.log(2);
				$('#lowBid').hide(); 
				$.ajax({
					type: "POST",
					url: "bid/add",
					dataType: "json",
					data:  $(this).serialize() ,
						success:
							function(data) {
								//console.log(data);
								if(data == "true"){
									$('#addBidSuccess').show(); 
									$('#addBidError').hide();
									$("#bidder_price").val("");
									setTimeout(function(){ $('#addBidSuccess').hide();}, 2200);	
								}else{
									$('#addBidSuccess').hide(); 
									$('#addBidError').show();
								}
							},
						error:
							function(data){
								//console.log(data);		
								//console.log("false");
								$('#addBidSuccess').hide();									
								$('#addBidError').show();									
							}
				});
			}else{
				$('#lowBid').show(); 
			}
		}
	});
	$('#sendbid-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(1);
		} else {
			//console.log(0);
			e.preventDefault();
			if($("#bidder_price").val() >= $("#auction_min_price").val()){
				//console.log(2);
				$('#lowBid').hide(); 
				$.ajax({
					type: "POST",
					url: "../../bid/add",
					dataType: "json",
					data:  $(this).serialize() ,
						success:
							function(data) {
								//console.log(data);
								if(data == "true"){
									$('#addBidSuccess').show(); 
									$('#addBidError').hide();
									$("#bidder_price").val("");
									setTimeout(function(){ $('#addBidSuccess').hide();}, 2200);	
								}else{
									$('#addBidSuccess').hide(); 
									$('#addBidError').show();
								}
							},
						error:
							function(data){
								//console.log(data);		
								//console.log("false");
								$('#addBidSuccess').hide();									
								$('#addBidError').show();									
							}
				});
			}else{
				$('#lowBid').show(); 
			}
		}
	});
})();
function shareNow(){

     var share = {
            method: 'stream.share',
            quote: 'Auction Name: '+name+' \n Description: '+desc+'\n Picture: https://tusk.nu/images/auctions/'+user_id+'/'+pic,
            u: 'https://tusk.nu/auction/view/'+id
     };
 
     FB.ui(share, function(response) { console.log(response); });
}
function showCurrency(id){
	//console.log(id);
	$("#currencies" + id).toggle();
}
function showFeaCurrency(id){
	//console.log(id);
	$("#feature_currencies" + id).toggle();
}
$(document).ready(function() {
	$('.btn_count').click();				
	$('.recent_count').click();				
});
function showAuctionDetails(timer,id){
		var URL = "";
		var href = window.location.href;
		var hostname = window.location.hostname;
		//console.log(href);
		//console.log(hostname + "/auction");
		$("#pic-1").hide();
		$("#pic-2").hide();
		$("#pic-3").hide();
		$("#pic-4").hide();
		document.getElementById('pic-img-1').src = "";
		document.getElementById('pic-img-2').src = "";
		document.getElementById('pic-img-3').src = "";
		document.getElementById('pic-img-4').src = "";
		if(href == hostname + "/auction" || href == hostname + "/auction#"){
			URL = "auction/get";
			//console.log(URL);
		}else{
			URL = "../../auction/get";
			//console.log(URL);
		}
			$.ajax({
				type: "POST",
				url: URL,
				dataType: "json",
				data:  {id:id} ,
					success:
						function(data) {
							//console.log(data);
							var base_url = $("#base_url").val();
							$("#auction_id").val(data[0].u_id);
							$("#item_id").val(data[0].u_id);
							$("#auc_name").html(data[0].item_name);
							$("#auction_min_price").val(data[0].min_price);
							$("#auc_price").html(data[0].min_price);
							if(data[0].designer_name != ""){
								$("#auc_designer_label").show();
								$("#auc_designer").html(data[0].designer_name);
							}else{
								$("#auc_designer_label").hide();
							}
							id = data[0].u_id;
							user_id = data[0].user_id;
							name = data[0].item_name;
							desc = data[0].item_desc;
							pic = data[0].main_pic;
							$("#auc_desc").html(data[0].item_desc);
							$("#auc_loc").html(data[0].location);
							
							if(data[0].ship_to_international == 1){
								$("#ship_i").show();
							}else{
								$("#ship_i").hide();
							}
							
							if(data[0].ship_to_local == 1){
								$("#ship_l").show();
							}else{
								$("#ship_l").hide();
							}
							
							if(data[0].pick_up == 1){
								$("#pick_u").show();
							}else{
								$("#pick_u").hide();
							}
							if(data[0].main_pic != ""){
								document.getElementById('pic-img-1').src = base_url +"images/auctions/"+data[0].user_id+"/"+data[0].main_pic;
								$("#pic-1").show();
							}
							if(data[0].other_pic1 != ""){
								document.getElementById('pic-img-2').src = base_url +"images/auctions/"+data[0].user_id+"/"+data[0].other_pic1;
								$("#pic-2").show();
							}
							if(data[0].other_pic2 != ""){
								document.getElementById('pic-img-3').src = base_url +"images/auctions/"+data[0].user_id+"/"+data[0].other_pic2;
								$("#pic-3").show();
							}
							if(data[0].other_pic3 != ""){
								document.getElementById('pic-img-4').src = base_url +"images/auctions/"+data[0].user_id+"/"+data[0].other_pic3;
								$("#pic-4").show();
							}
							
							if(data[1].length > 0){
								$("#tbodyTopFiveBid").empty();
								for(var x=0;x < data[1].length;x++){
									$("#topFiveBid").find('tbody')
										.append($('<tr>')
											.append($('<td>')
												.append(zeroPad(data[1][x].bidder_id , 10))
											)
											.append($('<td>')
												.append(data[1][x].bidder_price)
											)
										);
								}
								$("#topFiveBid").show();
								$("#notopfivebid").hide();
							}else{
								$("#topFiveBid").hide();
								$("#notopfivebid").show();
							}
							var clock;

							clock = $('#view_auction_clock').FlipClock({
								clockFace: 'DailyCounter',
								autoStart: false,
							});
											
							clock.setTime(timer);
							clock.setCountdown(true);
							clock.start();
							$("#showMore").modal("show");
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");									
						}
			});
			return false;
}
function showFeatureCountdown(time,id){
	//console.log(time);
	//console.log(id);
	var clock;

	clock = $('#feature_clock'+id).FlipClock({
		clockFace: 'DailyCounter',
		autoStart: false,
	});
				    
	clock.setTime(time);
	clock.setCountdown(true);
	clock.start();
}	
function saveVisitor(){
			$.ajax({
				type: "GET",
				url: "https://api.ipify.org?format=json",
				dataType: "json",
				success:
					function(data) {
						//console.log(data.ip);
						var ipAddress = data.ip; 
						$.ajax({
							type: "POST",
							url: "home/save_visitor",
							dataType: "json",
							data: {ip:ipAddress},
							success:
								function(data) {
									//console.log(data.ip);
								},
							error:
								function(data){
									//console.log("error");	
									//$("#loginInvalid").show();						
								}
						});
					},
				error:
					function(data){
						//console.log("error");	
						//$("#loginInvalid").show();						
					}
			});
}			
function showRecentCountdown(time,id){
	//console.log(time);
	//console.log(id);
	var clock;

	clock = $('#recent_clock'+id).FlipClock({
		clockFace: 'DailyCounter',
		autoStart: false,
	});
				    
	clock.setTime(time);
	clock.setCountdown(true);
	clock.start();
}		
function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}		