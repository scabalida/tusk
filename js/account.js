(function() {
	'use strict';
	$("#verifymenow").click(function(){
		$('#verifyMessage').modal("show");
		$.ajax({
			type: "POST",
			url: "account/sendVerification",
			dataType: "json",
			data:  $(this).serialize() ,
			success:
				function(data) {
					$('#verifyMessage').modal("show");
				},
			error:
				function(data){									
				}
		});
	});
	$('#add-user-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(1);
		} else {
			e.preventDefault();
			if($("#password").val() != $("#cpassword").val()){
				$('#diffPassError').show();
				//console.log(2);
			}else{
				//console.log(0);
				var email = $("#email").val();
				var fullname = $("#fullname").val();
				var bday = $("#bday").val();
				var cc_code = $("#cc_code").val();
				var zip = $("#zip").val();
				var country = $("#country").val();
				var address = $("#address").val();
				var contact1 = $("#contact1").val();
				var password = $("#password").val();
				$('#diffPassError').hide();
				$.ajax({
					type: "POST",
					url: "chckEmail",
					dataType: "json",
					data:{email:email},
					success:
						function(data) {
							if(data == "true"){
								$("#addEmailDuplicate").show();
							}else{
								e.preventDefault();
								$("#addEmailDuplicate").hide();
								$.ajax({
									type: "POST",
									url: "addUser",
									dataType: "json",
									data:{email:email,fullname:fullname,bday:bday,address:address,contact1:contact1,cc_code:cc_code,zip:zip,country:country,password:password},
										success:
											function(data) {
												console.log(data);
												if(data.status == "true"){
													$('#addUserSuccess').show(); 
													$('#addUserError').hide();
													$("#fullname").val("");
													$("#email").val("");
													$("#bday").val("");
													$("#address").val("");
													$("#contact1").val("");
													$("#zip").val("");
													$("#cc_code").val("");
													$("#country").val("");
													$("#password").val("");
													$("#cpassword").val("");
													window.location.href = "http://tusk.nu/account/registerPayment/"+data.id;
													setTimeout(function(){ $('#addUserSuccess').hide();}, 2200);	
												}else{
													$('#addUserSuccess').hide(); 
													$('#addUserError').show();
												}
											},
										error:
											function(data){
												//console.log(data);		
												//console.log("false");
												$('#addUserError').show();									
											}
								});

							}
						},
					error:
					function(data){
								
					}
				});
				
			}
		}
	});
	$('#update-user-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(1);
		} else {
			e.preventDefault();
			$.ajax({
				type: "POST",
				url: "updateUser",
				dataType: "json",
				data:  $(this).serialize() ,
					success:
						function(data) {
							console.log(data);
							if(data.status == "true"){
								$('#updateUserSuccess').show(); 
								$('#updateUserError').hide();
								setTimeout(function(){ $('#updateUserSuccess').hide();}, 2200);	
							}else{
								$('#updateUserSuccess').hide(); 
								$('#updateUserError').show();
							}
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
							$('#updateUserError').show();									
						}
			});
		}
	});
	$('#update-password-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(1);
		} else {
			e.preventDefault();
			if($("#password").val() != $("#cpassword").val()){
				$('#diffPassError').show();
				//console.log(2);
			}else{
				$('#diffPassError').hide();
				$.ajax({
					type: "POST",
					url: "updatePassword",
					dataType: "json",
					data:  $(this).serialize() ,
						success:
							function(data) {
								console.log(data);
								if(data.status == "true"){
									$('#updatePassSuccess').show(); 
									$('#updatePassError').hide();
									setTimeout(function(){ $('#updatePassSuccess').hide();}, 2200);	
								}else{
									$('#updatePassSuccess').hide(); 
									$('#updatePassError').show();
								}
							},
						error:
							function(data){
								//console.log(data);		
								//console.log("false");
								$('#updatePassError').show();									
							}
				});
			}
		}
	});
	cc_code();
})();

function userType(d){
	if(d == 1){
		return "Regular";
	}else if(d == 2){
		return "Donator";
	}else if(d == 3){
		return "VIP";
	}else if(d == 4){
		return "Updater";
	}else if(d == 5){
		return "Operator";
	}else if(d == 6){
		return "Admin";
	}else{
		return "Regular";
	}
	
}
function cc_code(){
	document.getElementById('cc_code').value = document.getElementById('cc_c').value;
	
}
function getMonthDesc(m){
	var mDesc = "";
	switch (m){
		case 1:
		mDesc = "January";
		break;
		case 2:
		mDesc = "February";
		break;
		case 3:
		mDesc = "March";
		break;
		case 4:
		mDesc = "April";
		break;
		case 5:
		mDesc = "May";
		break;
		case 6:
		mDesc = "June";
		break;
		case 7:
		mDesc = "July";
		break;
		case 8:
		mDesc = "August";
		break;
		case 9:
		mDesc = "September";
		break;
		case 10:
		mDesc = "October";
		break;
		case 11:
		mDesc = "November";
		break;
		case 12:
		mDesc = "December";
		break;
	}
	return mDesc;
}
function reformatDate(d){
	var dbDate = new Date(d);
	var yyyy = dbDate.getFullYear();
	var mm = dbDate.getMonth() + 1;
	var dd = dbDate.getDate();
	return getMonthDesc(mm)+" "+dd+", "+yyyy;
}

function setDate(n){
	var d = new Date(n);
	
	var month = d.getMonth();
	var mWord;
	switch(month){
		case 0:
        mWord = "January";
        break;
    case 1:
        mWord = "February";
        break;
	case 2:
        mWord = "March";
        break;
	case 3:
        mWord = "April";
        break;
	case 4:
        mWord = "May";
        break;
	case 5:
        mWord = "June";
        break;
	case 6:
        mWord = "July";
        break;
	case 7:
        mWord = "August";
        break;
	case 8:
        mWord = "September";
        break;
	case 9:
        mWord = "October";
        break;
	case 10:
        mWord = "November";
        break;
	case 11:
        mWord = "December";
        break;
	}
	var nDate = d.getDate();
	var nYear = d.getFullYear();
	
	return mWord+" "+nDate+", "+nYear;
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}
