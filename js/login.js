(function($){
	$('#login-form').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
							 
			$.ajax({
				type: "POST",
				url: "login/loginNow",
				dataType: "json",
				data:  $(this).serialize() ,
				success:
					function(data) {
						console.log(data);
						if(data == "0"){
							window.location.reload();
							console.log("true");
							$("#loginInvalid").hide();
							$("#accountExpired").hide();
							$("#notPaid").hide();
						}
						else if(data == "1"){
							console.log("false");
							$("#notPaid").show();
							$("#loginInvalid").hide();
							$("#accountExpired").hide();
						}else if(data == "2"){
							console.log("false");
							$("#loginInvalid").hide();
							$("#notPaid").hide();
							$("#accountExpired").show();
						}else if(data == "3"){
							console.log("false");
							$("#loginInvalid").show();
							$("#notPaid").hide();
							$("#accountExpired").hide();
						}
					},
				error:
					function(data){
						console.log("error");	
						//$("#loginInvalid").show();						
					}
			});
		}
	});

})(jQuery);
